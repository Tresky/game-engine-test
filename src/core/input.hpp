#ifndef input_hpp
#define input_hpp

#include <SFML/Graphics.hpp>
#include "bitmask.hpp"

class Input {
public:
  enum class Key {
    None = 0,
    Left = 1,
    Right = 2,
    Up = 3,
    Down = 4,
    Esc = 5,
    LBracket = 6,
    RBracket = 7,
    E = 8,
    R = 9
  };

  void update();

  bool isKeyPressed(Key _keycode);
  bool isKeyDown(Key _keycode);
  bool isKeyUp(Key _keycode);

private:
  Bitmask thisFrameKeys;
  Bitmask lastFrameKeys;
};

#endif
