#include "object.hpp"

Object::Object(SharedContext* _context)
  : context(_context)
  , queuedForRemoval(false) {
  transform = addComponent<C_Transform>();
  instanceId = addComponent<C_InstanceId>();
  tag = addComponent<C_Tag>();
}

void Object::awake() {
  for (const auto& component : components) {
    component->awake();
  }
}

void Object::start() {
  for (const auto& component : components) {
    component->start();
  }
}

void Object::update(float _deltaTime) {
  for (const auto& component : components) {
    component->update(_deltaTime);
  }
}

void Object::lateUpdate(float _deltaTime) {
  for (const auto& component : components) {
    component->lateUpdate(_deltaTime);
  }
}

void Object::draw(Window& _window) {
  drawable->draw(_window);
}

void Object::queueForRemoval() {
  queuedForRemoval = true;
}

bool Object::isQueuedForRemoval() {
  return queuedForRemoval;
}

std::shared_ptr<C_Drawable> Object::getDrawable() {
  return drawable;
}

void Object::onCollisionEnter(std::shared_ptr<C_BoxCollider> other) {
  for (const auto& component : collidables) {
    component->onCollisionEnter(other);
  }
}

void Object::onCollisionStay(std::shared_ptr<C_BoxCollider> other) {
  for (const auto& component : collidables) {
    component->onCollisionStay(other);
  }
}

void Object::onCollisionExit(std::shared_ptr<C_BoxCollider> other) {
  for (const auto& component : collidables) {
    component->onCollisionExit(other);
  }
}
