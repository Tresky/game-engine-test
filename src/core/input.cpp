#include "input.hpp"

void Input::update() {
  lastFrameKeys.setMask(thisFrameKeys);

  thisFrameKeys.setBit(
    (int)Key::Left,
    sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ||
      sf::Keyboard::isKeyPressed(sf::Keyboard::A)
  );

  thisFrameKeys.setBit(
    (int)Key::Right,
    sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ||
      sf::Keyboard::isKeyPressed(sf::Keyboard::D)
  );

  thisFrameKeys.setBit(
    (int)Key::Up,
    sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ||
      sf::Keyboard::isKeyPressed(sf::Keyboard::W)
  );

  thisFrameKeys.setBit(
    (int)Key::Down,
    sf::Keyboard::isKeyPressed(sf::Keyboard::Down) ||
      sf::Keyboard::isKeyPressed(sf::Keyboard::S)
  );

  thisFrameKeys.setBit(
    (int)Key::Esc,
    sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
  );

  thisFrameKeys.setBit(
    (int)Key::LBracket,
    sf::Keyboard::isKeyPressed(sf::Keyboard::LBracket)
  );

  thisFrameKeys.setBit(
    (int)Key::RBracket,
    sf::Keyboard::isKeyPressed(sf::Keyboard::RBracket)
  );

  thisFrameKeys.setBit(
    (int)Key::E,
    sf::Keyboard::isKeyPressed(sf::Keyboard::E)
  );

  thisFrameKeys.setBit(
    (int)Key::R,
    sf::Keyboard::isKeyPressed(sf::Keyboard::R)
  );
}

bool Input::isKeyPressed(Key _keycode) {
  return thisFrameKeys.getBit((int)_keycode);
}

bool Input::isKeyDown(Key _keycode) {
  bool lastFrame = lastFrameKeys.getBit((int)_keycode);
  bool thisFrame = thisFrameKeys.getBit((int)_keycode);
  return thisFrame && !lastFrame;
}

bool Input::isKeyUp(Key _keycode) {
    bool lastFrame = lastFrameKeys.getBit((int)_keycode);
    bool thisFrame = thisFrameKeys.getBit((int)_keycode);
    
    return !thisFrame && lastFrame;
}