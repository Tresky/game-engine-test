#include "c_interactable_talking.hpp"

C_InteractableTalking::C_InteractableTalking(Object* owner)
  : Component(owner)
  , textToSay("Bye!") {}

void C_InteractableTalking::onInteraction(Object* other) {
  Debug::log(textToSay);
  std::shared_ptr<Object> labelObj = std::make_shared<Object>(owner->context);

  labelObj->transform->setParent(owner->transform);
  labelObj->transform->setPosition(0.f, -80.f);

  auto label = labelObj->addComponent<C_UIWorldLabel>();
  label->setDrawLayer(DrawLayer::UI);

  label->setBackgroundSize(sf::Vector2f(120, 120));
  label->setBackgroundColor(sf::Color::Black);

  label->setFontColor(sf::Color::White);
  label->setText(textToSay);

  owner->context->objects->add(labelObj);
}
