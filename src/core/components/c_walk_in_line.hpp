#ifndef c_walk_in_line_hpp
#define c_walk_in_line_hpp

#include "../component.hpp"
#include "c_interactable.hpp"
#include "c_velocity.hpp"

class C_WalkInLine : public Component, public C_Interactable {
public:
  C_WalkInLine(Object* owner);

  void awake() override;

  void onInteraction(Object* owner) override;

private:
  std::shared_ptr<C_Velocity> velocity;
  float moveSpeed;
};

#endif
