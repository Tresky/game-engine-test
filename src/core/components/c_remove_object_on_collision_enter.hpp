#ifndef c_remove_object_on_collision_enter_hpp
#define c_remove_object_on_collision_enter_hpp

#include "../component.hpp"
#include "c_collidable.hpp"

class C_RemoveObjectOnCollisionEnter : public Component, public C_Collidable {
public:
  C_RemoveObjectOnCollisionEnter(Object* owner);

  void onCollisionEnter(std::shared_ptr<C_BoxCollider> other) override;
};

#endif