#include "c_drawable.hpp"

C_Drawable::C_Drawable()
  : sortOrder(0)
  , layer(DrawLayer::Default) {}

C_Drawable::~C_Drawable() {}

void C_Drawable::setSortOrder(int _order) {
  sortOrder = _order;
}

int C_Drawable::getSortOrder() const {
  return sortOrder;
}

void C_Drawable::setDrawLayer(DrawLayer _drawLayer) {
  layer = _drawLayer;
}

DrawLayer C_Drawable::getDrawLayer() const {
  return layer;
}
