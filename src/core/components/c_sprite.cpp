#include "c_sprite.hpp"
#include "../object.hpp"

C_Sprite::C_Sprite(Object* _owner)
  : Component(_owner)
  , currentTextureId(-1) {}

void C_Sprite::load(const std::string& _filePath) {
  int textureId = owner->context->textureAllocator->add(_filePath);
  if (textureId >= 0 && textureId != currentTextureId) {
    currentTextureId = textureId;
    std::shared_ptr<sf::Texture> texture = owner->context->textureAllocator->get(textureId);
    sprite.setTexture(*texture);
  }
}

void C_Sprite::load(int _textureId) {
  if (_textureId >= 0 && _textureId != currentTextureId) {
    currentTextureId = _textureId;
    std::shared_ptr<sf::Texture> texture = owner->context->textureAllocator->get(_textureId);
    sprite.setTexture(*texture);
  }
}

void C_Sprite::draw(Window& _window) {
  _window.draw(sprite);

  const sf::Vector2f pos = owner->transform->getPosition();
  const sf::Vector2f& spriteScale = sprite.getScale();
  const sf::IntRect& spriteBounds = sprite.getTextureRect();
  // Debug::drawRect(
  //   sf::FloatRect(
  //     pos.x - ((abs(spriteBounds.width) * 0.5f) * spriteScale.x),
  //     pos.y - ((abs(spriteBounds.height) * 0.5f) * spriteScale.y),
  //     abs(spriteBounds.width) * spriteScale.x, abs(spriteBounds.height) * spriteScale.y
  //   ), sf::Color::Green
  // );
}

//TODO: implement static sprites for level tiles etc.
void C_Sprite::lateUpdate(float _deltaTime) {
  sf::Vector2f pos = owner->transform->getPosition();
  const sf::IntRect& spriteBounds = sprite.getTextureRect();
  const sf::Vector2f& spriteScale = sprite.getScale();
  sprite.setPosition(
    pos.x - ((abs(spriteBounds.width) * 0.5f) * spriteScale.x),
    pos.y - ((abs(spriteBounds.height) * 0.5f) * spriteScale.y)
  );
}

bool C_Sprite::continueToDraw() const {
  return !owner->isQueuedForRemoval();
}

void C_Sprite::setTextureRect(int _x, int _y, int _width, int _height) {
  sprite.setTextureRect(sf::IntRect(_x, _y, _width, _height));
}

void C_Sprite::setTextureRect(const sf::IntRect& _rect) {
  sprite.setTextureRect(_rect);
}

void C_Sprite::setScale(float _x, float _y) {
  sprite.setScale(_x, _y);
}
