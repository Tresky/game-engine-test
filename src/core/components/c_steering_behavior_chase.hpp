#ifndef c_steering_behavior_chase_hpp
#define c_steering_behavior_chase_hpp

#include "c_steering_behavior.hpp"

class C_SteeringBehaviorChase : public C_SteeringBehavior {
public:
  C_SteeringBehaviorChase(Object* owner);

  const sf::Vector2f getForce() override;

  void setTarget(Tag tag);
  void setSightRadius(float radius);
  void setChaseSpeed(float speed);

private:

  float sightRadius;
  float chaseSpeed;
  Tag targetTag;
};

#endif
