#include "c_collider.hpp"

C_Collider::C_Collider(Object* _owner)
  : Component(_owner), layer(CollisionLayer::Default) {}

C_Collider::~C_Collider() {}

CollisionLayer C_Collider::getLayer() const {
  return layer;
}

void C_Collider::setLayer(CollisionLayer _layer) {
  this->layer = _layer;
}
