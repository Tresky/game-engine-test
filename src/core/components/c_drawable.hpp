#ifndef c_drawable_hpp
#define c_drawable_hpp

#include <SFML/Graphics.hpp>

#include "../window.hpp"

enum class DrawLayer {
  Default,
  Background,
  Foreground,
  Entities,
  UI
};

class C_Drawable {
public:
  C_Drawable();
  virtual ~C_Drawable();

  virtual void draw(Window& _window) = 0;
  virtual bool continueToDraw() const = 0;

  void setSortOrder(int _order);
  int getSortOrder() const;

  void setDrawLayer(DrawLayer _drawLayer);
  DrawLayer getDrawLayer() const;

private:
  int sortOrder;
  DrawLayer layer;
};

#endif
