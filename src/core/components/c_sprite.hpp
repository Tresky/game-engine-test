#ifndef c_sprite_hpp
#define c_sprite_hpp

#include "../component.hpp"
#include "../resource_allocator.hpp"
#include "c_drawable.hpp"
#include "c_transform.hpp"

class C_Sprite : public Component, public C_Drawable
{
public:
  C_Sprite(Object* _owner);

  void load(const std::string& _filePath);  
  void load(int _textureId);

  void draw(Window& _window) override; 
  void lateUpdate(float _deltaTime) override;
  bool continueToDraw() const override;

  void setTextureRect(int _x, int _y, int _width, int _height);
  void setTextureRect(const sf::IntRect& _rect);

  void setScale(float _x, float _y);

private:
  sf::Sprite sprite;
  int currentTextureId;
};

#endif
