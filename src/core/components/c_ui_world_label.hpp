#ifndef c_ui_world_label_hpp
#define c_ui_world_label_hpp

#include "../component.hpp"
#include "c_drawable.hpp"

class C_UIWorldLabel : public Component, public C_Drawable {
public:
  C_UIWorldLabel(Object* owner);

  void start() override;

  void draw(Window& window) override;
  bool continueToDraw() const override;

  void lateUpdate(float deltaTime) override;

  void setText(const std::string& text);
  void setBackgroundSize(const sf::Vector2f& size);

  void setFontColor(const sf::Color& color);
  void setBackgroundColor(const sf::Color& color);

private:
  sf::Text text;
  sf::RectangleShape background;
};

#endif
