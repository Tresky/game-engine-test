#ifndef c_projectile_attack_hpp
#define c_projectile_attack_hpp

#include <unordered_map>
#include "../enum_class_hash.hpp"
#include "../object_collection.hpp"
#include "../working_directory.hpp"
#include "../component.hpp"
#include "../input.hpp"
#include "c_animation.hpp"
#include "c_remove_object_on_collision_enter.hpp"

class C_ProjectileAttack : public Component {
public:
  C_ProjectileAttack(Object* _owner);

  void awake() override;
  void update(float _deltaTime) override;

  void start() override;
  void spawnProjectile();

private:
  static std::unordered_map<FacingDirection, sf::IntRect, EnumClassHash> textureDirectionBindings;
  static std::unordered_map<FacingDirection, sf::Vector2f, EnumClassHash> offsetDirectionBindings;
  static std::unordered_map<FacingDirection, sf::Vector2f, EnumClassHash> velocityDirectionBindings;
  std::shared_ptr<C_Animation> animation;
  std::shared_ptr<C_Direction> direction;
  float projectileVelocity;
  int projectileTextureId;
};

#endif
