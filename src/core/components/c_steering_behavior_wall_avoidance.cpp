#include "c_steering_behavior_wall_avoidance.hpp"

C_SteeringBehaviorWallAvoidance::C_SteeringBehaviorWallAvoidance(Object* owner)
  : C_SteeringBehavior(owner)
  , lookDistance(100.f) {}

void C_SteeringBehaviorWallAvoidance::awake() {
  velocity = owner->getComponent<C_Velocity>();
}

const sf::Vector2f C_SteeringBehaviorWallAvoidance::getForce() {
  sf::Vector2f force(0, 0);

  const sf::Vector2f& pos = owner->transform->getPosition();
  const sf::Vector2f& vel = velocity->get();

  if (vel.x == 0 && vel.y == 0) {
    return force;
  }

  const sf::Vector2f& velNorm = normalize(vel);

  // Front ray
  auto frontResult = owner->context->raycast->cast(pos, pos + (velNorm * lookDistance), CollisionLayer::Tile);
  if (frontResult.collision != nullptr) {
    Debug::drawLine(pos, pos + (velNorm * lookDistance), sf::Color::Red);
  } else {
    Debug::drawLine(pos, pos + (velNorm * lookDistance));
  }

  // Convert from radians to degrees
  const float angle = 30.f / 57.2958f;
  const sf::Vector2f to = (velNorm * lookDistance);

  // Side Rays
  sf::Vector2f directions[] = {
      sf::Vector2f(
      (to.x * cosf(angle)) - (to.y * sinf(angle)),
      (to.y * cosf(angle)) + (to.x * sinf(angle))),
      sf::Vector2f(
      (to.x * cosf(-angle)) - (to.y * sinf(-angle)),
      (to.y * cosf(-angle)) + (to.x * sinf(-angle)))
  };

  for (int i = 0; i < 2; i++) {
    auto result = owner->context->raycast->cast(pos, pos + directions[1], CollisionLayer::Tile);
    if (result.collision != nullptr) {
      Debug::drawLine(pos, pos + directions[i], sf::Color::Red);
    } else {
      Debug::drawLine(pos, pos + directions[i], sf::Color::Yellow);
    }
  }

  return force;
}