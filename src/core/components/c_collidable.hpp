#ifndef c_collidable_hpp
#define c_collidable_hpp

#include <memory>
#include "c_box_collider.hpp"

class C_Collidable {
public:
  virtual void onCollisionEnter(std::shared_ptr<C_BoxCollider> other) {};
  virtual void onCollisionStay(std::shared_ptr<C_BoxCollider> other) {};
  virtual void onCollisionExit(std::shared_ptr<C_BoxCollider> other) {};
};

#endif
