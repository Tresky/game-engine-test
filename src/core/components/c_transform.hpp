#ifndef c_transform_hpp
#define c_transform_hpp

#include "../component.hpp"

class C_Transform : public Component {
public:
  C_Transform(Object* _owner);

  void setPosition(float _x, float _y);
  void setPosition(const sf::Vector2f& _pos);

  void addPosition(float _x, float _y);
  void addPosition(const sf::Vector2f& _pos);

  void setX(float _x);
  void setY(float _y);

  void addX(float _x);
  void addY(float _y);

  sf::Vector2f getPosition() const;

  void setStatic(bool _isStatic);
  bool isStatic() const;

  void setParent(std::shared_ptr<C_Transform> parent);
  const std::shared_ptr<C_Transform> getParent() const;

  void addChild(std::shared_ptr<C_Transform> child);
  void removeChild(std::shared_ptr<C_Transform> child);
  const std::vector<std::shared_ptr<C_Transform>>& getChildren() const;

private:
  sf::Vector2f position;
  bool isStaticTransform;

  std::shared_ptr<C_Transform> parent;
  std::vector<std::shared_ptr<C_Transform>> children;
};

#endif
