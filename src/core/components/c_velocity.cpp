#include "c_velocity.hpp"
#include "../object.hpp"

C_Velocity::C_Velocity(Object* _owner)
  : Component(_owner)
  , maxVelocity(500.f, 500.f) {}

void C_Velocity::update(float _deltaTime) {
  owner->transform->addPosition(velocity * _deltaTime);
}

void C_Velocity::set(const sf::Vector2f& _vel) {
  velocity = _vel;
  clampVelocity();
}

void C_Velocity::set(float _x, float _y) {
  velocity.x = _x;
  velocity.y = _y;
  clampVelocity();
}

const sf::Vector2f& C_Velocity::get() const {
  return velocity;
}

void C_Velocity::clampVelocity() {
  if (fabs(velocity.x) > maxVelocity.x) {
    velocity.x = velocity.x > 0.f ? maxVelocity.x : -maxVelocity.x;
  }
  if (fabs(velocity.y) > maxVelocity.y) {
    velocity.y = velocity.y > 0.f ? maxVelocity.y : -maxVelocity.y;
  }
}
