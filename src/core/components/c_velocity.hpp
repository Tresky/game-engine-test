#ifndef c_velocity_hpp
#define c_velocity_hpp

#include <cmath>
#include "../component.hpp"

class C_Velocity : public Component {
public:
  C_Velocity(Object* _owner);

  void update(float _deltaTime) override;

  void set(const sf::Vector2f& _vel);
  void set(float _x, float _y);
  const sf::Vector2f& get() const;

private:
  void clampVelocity();

  sf::Vector2f velocity;
  sf::Vector2f maxVelocity;
};

#endif
