#ifndef c_tag_hpp
#define c_tag_hpp

#include "../component.hpp"

enum class Tag {
  Default,
  Player,
  NPC
};

class C_Tag : public Component {
public:
  C_Tag(Object* owner);

  Tag get() const;
  void set(Tag tag);

  bool compare(std::shared_ptr<C_Tag> other) const;
  bool compare(Tag other) const;

private:
  Tag tag;
};

#endif
