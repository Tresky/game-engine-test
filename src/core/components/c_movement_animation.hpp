#ifndef c_movement_animation_hpp
#define c_movement_animation_hpp

#include "../component.hpp"
#include "c_velocity.hpp"
#include "c_animation.hpp"

class C_MovementAnimation : public Component {
public:
  C_MovementAnimation(Object* _owner);

  void awake() override;
  void update(float _deltaTime) override;

private:
  std::shared_ptr<C_Velocity> velocity;
  std::shared_ptr<C_Animation> animation;
};

#endif
