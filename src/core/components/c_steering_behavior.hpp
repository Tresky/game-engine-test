#ifndef c_steering_behavior_hpp
#define c_steering_behavior_hpp

#include <cmath>
#include "../component.hpp"
#include "../shared_context.hpp"
#include "../quadtree.hpp"
#include "c_tag.hpp"

class C_SteeringBehavior : public Component {
public:
  C_SteeringBehavior(Object* owner);

  virtual const sf::Vector2f getForce() = 0;

  unsigned int getWeight() const;
  void setWeight(unsigned int weight);

protected:
  Object* getEntityInSight(float sightRadius, Tag tag) const;
  std::vector<Object*> getEntitiesInSight(float sightRadius, Tag tag) const;

  //TODO: move to a separate class.
  inline sf::Vector2f normalize(const sf::Vector2f& v) {
    float l = magnitude(v);

    if (l != 0) {
      return v / 1.f;
    }
    return v;
  }

  //TODO: move to a separate class
  inline float magnitude(const sf::Vector2f& v) {
    return sqrt((v.x * v.x) + (v.y * v.y));
  }

private:
  unsigned int weight;
};

#endif
