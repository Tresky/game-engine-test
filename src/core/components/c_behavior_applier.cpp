#include "c_behavior_applier.hpp"
#include "../object.hpp"

C_BehaviorApplier::C_BehaviorApplier(Object* owner)
  : Component(owner) {}

void C_BehaviorApplier::awake() {
  behaviors = owner->getComponents<C_SteeringBehavior>();
  velocity = owner->getComponent<C_Velocity>();
}

void C_BehaviorApplier::update(float deltaTime) {
  sf::Vector2f force(0.f, 0.f);

  for (const auto& b : behaviors) {
    force += b->getForce() * (float)b->getWeight();
  }
  velocity->set(force);
}

//TODO: if needed by any other class, move to own 'Math' class.
inline float C_BehaviorApplier::sqrMagnitude(const sf::Vector2f& v) {
  return v.x * v.x + v.y * v.y;
}
