#ifndef c_behavior_applier_hpp
#define c_behavior_applier_hpp

#include "../component.hpp"
#include "c_steering_behavior.hpp"
#include "c_velocity.hpp"

class C_BehaviorApplier : public Component {
public:
  C_BehaviorApplier(Object* owner);

  void awake() override;
  void update(float deltaTime) override;

private:
  inline float sqrMagnitude(const sf::Vector2f& v);

  std::vector<std::shared_ptr<C_SteeringBehavior>> behaviors;
  std::shared_ptr<C_Velocity> velocity;
};

#endif
