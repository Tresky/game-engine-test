#ifndef c_interact_with_objects
#define c_interact_with_objects

#include "../component.hpp"
#include "../raycast.hpp"
#include "c_direction.hpp"
#include "c_interactable.hpp"

class C_InteractWithObjects : public Component {
public:
  C_InteractWithObjects(Object* owner);

  void awake() override;
  void update(float deltaTime) override;

private:
  std::shared_ptr<C_Direction> direction;
  float interactionDistance;
};

#endif
