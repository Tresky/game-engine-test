#ifndef c_animation_hpp
#define c_animation_hpp

#include <unordered_map>
#include "../enum_class_hash.hpp"
#include "../component.hpp"
#include "../animation.hpp"
#include "c_sprite.hpp"
#include "c_direction.hpp"

enum AnimationState { None, Idle, Walk, Projectile };

using AnimationList = std::unordered_map<FacingDirection, std::shared_ptr<Animation>, EnumClassHash>;

class C_Animation : public Component {
public:
  C_Animation(Object* _owner);

  void awake() override;
  void update(float _deltaTime) override;

  void addAnimation(AnimationState _state, AnimationList& _animationList);
  void addAnimationAction(AnimationState _state, FacingDirection _dir, int _frame, AnimationAction _action);

  void setAnimationState(AnimationState _state);
  const AnimationState& getAnimationState() const;

  void setAnimationDirection(FacingDirection _dir);

private:
  std::shared_ptr<C_Sprite> sprite;
  std::shared_ptr<C_Direction> direction;
  std::unordered_map<AnimationState, AnimationList, EnumClassHash> animations;
  std::pair<AnimationState, std::shared_ptr<Animation>> currentAnimation;
  FacingDirection currentDirection;
};

#endif
