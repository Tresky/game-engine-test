#ifndef c_keyboard_movement_hpp
#define c_keyboard_movement_hpp

#include "../component.hpp"
#include "../input.hpp"
#include "c_velocity.hpp"
#include "c_animation.hpp"

class C_KeyboardMovement : public Component {
public:
  C_KeyboardMovement(Object* _owner);

  void setMovementSpeed(float _moveSpeed);
  void update(float _deltaTime) override;
  void awake() override;

private:
  float moveSpeed;
  std::shared_ptr<C_Velocity> velocity;
  std::shared_ptr<C_Animation> animation;
};

#endif
