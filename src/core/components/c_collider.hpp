#ifndef c_collider_hpp
#define c_collider_hpp

#include <memory>
#include <SFML/Graphics.hpp>

#include "../component.hpp"

enum class CollisionLayer {
  Default = 1,    // bit 0
  Player = 2,     // bit 1
  Tile = 3,       // bit 2
  Projectile = 4, // bit 3
  NPC = 5         // bit 4
};

struct Manifold {
  bool colliding = false;
  const sf::FloatRect* other;
};

class C_Collider : public Component {
public:
  C_Collider(Object* _owner);
  ~C_Collider();

  virtual Manifold intersects(std::shared_ptr<C_Collider> _other) = 0;
  virtual void resolveOverlap(const Manifold& _m) = 0;

  CollisionLayer getLayer() const;
  void setLayer(CollisionLayer _layer);

private:
  CollisionLayer layer;
};

#endif
