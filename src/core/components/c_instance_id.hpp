#ifndef c_instance_id_hpp
#define c_instance_id_hpp

#include "../component.hpp"

class C_InstanceId : public Component {
public:
  C_InstanceId(Object* _owner);
  ~C_InstanceId();

  unsigned int get() const;

private:
  static unsigned int count;
  unsigned id;
};

#endif
