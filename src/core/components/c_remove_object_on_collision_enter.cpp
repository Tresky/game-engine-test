#include "c_remove_object_on_collision_enter.hpp"
#include "../object.hpp"

C_RemoveObjectOnCollisionEnter::C_RemoveObjectOnCollisionEnter(Object* owner)
  : Component(owner) {}

void C_RemoveObjectOnCollisionEnter::onCollisionEnter(std::shared_ptr<C_BoxCollider> other) {
  owner->queueForRemoval();
}