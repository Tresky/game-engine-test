#include "c_transform.hpp"
#include "../object.hpp"

C_Transform::C_Transform(Object* _owner)
  : Component(_owner)
  , position(0.f, 0.f)
  , isStaticTransform(false) {}

void C_Transform::setPosition(float _x, float _y) {
  position.x = _x;
  position.y = _y;
}

void C_Transform::setPosition(const sf::Vector2f& _pos) {
  position = _pos;
}

void C_Transform::addPosition(float _x, float _y) {
  position.x += _x;
  position.y += _y;
}

void C_Transform::addPosition(const sf::Vector2f& _pos) {
  position += _pos;
}

void C_Transform::setX(float _x) {
  position.x = _x;
}

void C_Transform::setY(float _y) {
  position.y = _y;
}

void C_Transform::addX(float _x) {
  position.x += _x;
}

void C_Transform::addY(float _y) {
  position.y += _y;
}

sf::Vector2f C_Transform::getPosition() const {
  return parent == nullptr ? position : parent->getPosition() + position;
}

void C_Transform::setStatic(bool _isStatic) {
  isStaticTransform = _isStatic;
}

bool C_Transform::isStatic() const {
  return isStaticTransform;
}

void C_Transform::setParent(std::shared_ptr<C_Transform> parent) {
  this->parent = parent;
  this->parent->addChild(owner->transform);
}

const std::shared_ptr<C_Transform> C_Transform::getParent() const {
  return parent;
}

void C_Transform::addChild(std::shared_ptr<C_Transform> child) {
  children.push_back(child);
}

void C_Transform::removeChild(std::shared_ptr<C_Transform> child) {
  auto objIterator = children.begin();
  while (objIterator != children.end()) {
    auto obj = **objIterator;

    if (obj.owner->instanceId->get() == child->owner->instanceId->get()) {
      objIterator = children.erase(objIterator);
      break;
    } else {
      ++objIterator;
    }
  }
}

const std::vector<std::shared_ptr<C_Transform>>& C_Transform::getChildren() const {
  return children;
}
