#include "c_walk_in_line.hpp"
#include "../object.hpp"

C_WalkInLine::C_WalkInLine(Object* owner)
  : Component(owner)
  , moveSpeed(100.f) {}

void C_WalkInLine::awake() {
  velocity = owner->getComponent<C_Velocity>();
}

void C_WalkInLine::onInteraction(Object *other) {
  velocity->set(moveSpeed, 0.f);
}
