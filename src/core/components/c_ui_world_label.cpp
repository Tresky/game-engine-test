#include "c_ui_world_label.hpp"
#include "../object.hpp"

C_UIWorldLabel::C_UIWorldLabel(Object* owner)
  : Component(owner) {}

void C_UIWorldLabel::start() {
  const int fontId = owner->context->fontAllocator->add(owner->context->workingDir->get() + "media/Joystix Monospace.ttf");
  std::shared_ptr<sf::Font> font = owner->context->fontAllocator->get(fontId);
  text.setFont(*font);
}

void C_UIWorldLabel::draw(Window& window) {
  window.draw(background);
  window.draw(text);
}

bool C_UIWorldLabel::continueToDraw() const {
  return !owner->isQueuedForRemoval();
}

//TODO: UI elements do not often move so we should not need to query the position every frame
void C_UIWorldLabel::lateUpdate(float deltaTime) {
  sf::Vector2f pos = owner->transform->getPosition();
  const sf::FloatRect& backBounds = background.getLocalBounds();
  const sf::Vector2f centeredPosition = sf::Vector2f(pos.x - (backBounds.width * 0.5f), pos.y - (backBounds.height * 0.5f));
  background.setPosition(centeredPosition);
  text.setPosition(centeredPosition);
}

void C_UIWorldLabel::setText(const std::string& text) {
  this->text.setString(text);
}

void C_UIWorldLabel::setBackgroundSize(const sf::Vector2f& size) {
  background.setSize(size);
}

void C_UIWorldLabel::setFontColor(const sf::Color& color) {
  text.setFillColor(color);
  text.setOutlineColor(color);
}

void C_UIWorldLabel::setBackgroundColor(const sf::Color& color) {
  background.setFillColor(color);
}
