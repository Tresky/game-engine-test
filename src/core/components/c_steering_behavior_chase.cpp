#include "c_steering_behavior_chase.hpp"

C_SteeringBehaviorChase::C_SteeringBehaviorChase(Object* owner)
  : C_SteeringBehavior(owner)
  , sightRadius(500.f)
  , chaseSpeed(70.f)
  , targetTag(Tag::Default) {}

const sf::Vector2f C_SteeringBehaviorChase::getForce() {
  sf::Vector2f force(0, 0);

  Object* target = getEntityInSight(sightRadius, targetTag);

  if (target != nullptr) {
    const sf::Vector2f& pos = owner->transform->getPosition();
    const sf::Vector2f& targetPos = target->transform->getPosition();
    const sf::Vector2f toTarget = targetPos - pos;
    force = normalize(toTarget) * chaseSpeed;
  }

  return force;
}

void C_SteeringBehaviorChase::setSightRadius(float radius) {
  sightRadius = radius;
}

void C_SteeringBehaviorChase::setChaseSpeed(float speed) {
  chaseSpeed = speed;
}

void C_SteeringBehaviorChase::setTarget(Tag tag) {
  targetTag = tag;
}
