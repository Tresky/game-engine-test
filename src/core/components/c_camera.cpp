#include "c_camera.hpp"
#include "../object.hpp"

C_Camera::C_Camera(Object* _owner) : Component(_owner) {}

void C_Camera::lateUpdate(float _deltaTime) {
  sf::View view = owner->context->window->getView();
  const sf::Vector2f& targetPos = owner->transform->getPosition();
  view.setCenter(targetPos.x, targetPos.y);
  owner->context->window->setView(view);
}
