#include "c_instance_id.hpp"

unsigned int C_InstanceId::count = 0;

C_InstanceId::C_InstanceId(Object* _owner)
  : Component(_owner)
  , id(count++) {}

C_InstanceId::~C_InstanceId() {}

unsigned int C_InstanceId::get() const {
  return id;
}
