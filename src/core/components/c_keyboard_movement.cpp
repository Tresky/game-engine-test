#include "c_keyboard_movement.hpp"
#include "../object.hpp"

C_KeyboardMovement::C_KeyboardMovement(Object* _owner)
  : Component(_owner)
  , moveSpeed(300.f) {}

void C_KeyboardMovement::setMovementSpeed(float _moveSpeed) {
  this->moveSpeed = _moveSpeed;
}

void C_KeyboardMovement::update(float _deltaTime) {
  if (owner->context->input == nullptr) {
    return;
  }

  if (animation->getAnimationState() == AnimationState::Projectile) {
    velocity->set(0.f, 0.f);
    return;
  }

  float xMove = 0;
  if (owner->context->input->isKeyPressed(Input::Key::Left)) {
    xMove = -moveSpeed;
  } else if (owner->context->input->isKeyPressed(Input::Key::Right)) {
    xMove = moveSpeed;
  }

  float yMove = 0;
  if (owner->context->input->isKeyPressed(Input::Key::Up)) {
    yMove = -moveSpeed;
  } else if (owner->context->input->isKeyPressed(Input::Key::Down)) {
    yMove = moveSpeed;
  }

  velocity->set(xMove, yMove);
}

void C_KeyboardMovement::awake() {
  velocity = owner->getComponent<C_Velocity>();
  animation = owner->getComponent<C_Animation>();
}
