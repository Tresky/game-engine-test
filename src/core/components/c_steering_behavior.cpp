#include "c_steering_behavior.hpp"
#include "../object.hpp"

C_SteeringBehavior::C_SteeringBehavior(Object* owner)
  : Component(owner)
  , weight(1) {}

unsigned int C_SteeringBehavior::getWeight() const {
  return weight;
}

void C_SteeringBehavior::setWeight(unsigned int weight) {
  this->weight = weight;
}

Object* C_SteeringBehavior::getEntityInSight(float sightRadius, Tag tag) const {
  const float halfRadius = sightRadius * 0.5f;
  const sf::Vector2f& pos = owner->transform->getPosition();
  const sf::FloatRect searchRect(pos.x - halfRadius, pos.y - halfRadius, sightRadius, sightRadius);
  
  std::vector<std::shared_ptr<C_BoxCollider>> collisions = owner->context->collisionTree->search(searchRect);
  
  if(collisions.size() > 0) {
    const unsigned int thisId = owner->instanceId->get();

    for (auto c : collisions) {
      if(c->owner->instanceId->get() != thisId) {
        if(c->owner->tag->get() == tag) {
          return c->owner;
        }
      }
    }
  }
  
  return nullptr;
}


std::vector<Object*> C_SteeringBehavior::getEntitiesInSight(float sightRadius, Tag tag) const {
  std::vector<Object*> entitiesInSight;

  const float halfRadius = sightRadius * 0.5f;
  const sf::Vector2f& pos = owner->transform->getPosition();
  const sf::FloatRect searchRect(pos.x - halfRadius, pos.y - halfRadius, sightRadius, sightRadius);

  //TODO: we want to be able to use a circle as a search area.
  std::vector<std::shared_ptr<C_BoxCollider>> collisions = owner->context->collisionTree->search(searchRect);
  if (collisions.size() > 0) {
    const unsigned int thisId = owner->instanceId->get();
    for (auto c : collisions) {
      if (c->owner->instanceId->get() != thisId) {
        if (c->owner->tag->get() == tag) {
          //TODO: we don't want to calculate this for every behaviour every frame. Move to seperate sight component
          entitiesInSight.push_back(c->owner);
        }
      }
    }
  }
  return entitiesInSight;
}