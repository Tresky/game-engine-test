#ifndef c_interactable_hpp
#define c_interactable_hpp

#include "../object.hpp"

class C_Interactable {
public:
  virtual void onInteraction(Object* other) = 0;
};

#endif
