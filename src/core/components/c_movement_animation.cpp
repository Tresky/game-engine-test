#include "c_movement_animation.hpp"
#include "../object.hpp"

C_MovementAnimation::C_MovementAnimation(Object* _owner)
  : Component(_owner) {}

void C_MovementAnimation::awake() {
  velocity = owner->getComponent<C_Velocity>();
  animation = owner->getComponent<C_Animation>();
}

void C_MovementAnimation::update(float _deltaTime) {
  if (animation->getAnimationState() != AnimationState::Projectile) {
    const sf::Vector2f& currentVel = velocity->get();

    if (currentVel.x != 0.f || currentVel.y != 0.f) {
      animation->setAnimationState(AnimationState::Walk);
    } else {
      animation->setAnimationState(AnimationState::Idle);
    }
  }
}