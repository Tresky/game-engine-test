#ifndef c_interactable_talking_hpp
#define c_interactable_talking_hpp

#include "../component.hpp"
#include "../object_collection.hpp"
#include "c_interactable.hpp"
#include "c_ui_world_label.hpp"
#include "../debug.hpp"

class C_InteractableTalking : public Component, public C_Interactable {
public:
  C_InteractableTalking(Object* owner);

  void onInteraction(Object* other) override;

private:
  std::string textToSay;
};

#endif
