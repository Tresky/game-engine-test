#include <cmath>
#include "../object.hpp"
#include "c_box_collider.hpp"

C_BoxCollider::C_BoxCollider(Object* _owner)
  : C_Collider(_owner)
  , offset(sf::Vector2f(0.f, 0.f)) {}

void C_BoxCollider::setCollidable(const sf::FloatRect& _rect) {
  AABB = _rect;
  setPosition();
}

const sf::FloatRect& C_BoxCollider::getCollidable() {
  setPosition();
  return AABB;
}

void C_BoxCollider::setPosition() {
  const sf::Vector2f& pos = owner->transform->getPosition();
  AABB.left = pos.x - (AABB.width / 2) + offset.x;
  AABB.top = pos.y - (AABB.height / 2) + offset.y;
}

Manifold C_BoxCollider::intersects(std::shared_ptr<C_Collider> _other) {
  Manifold m;
  m.colliding = false;

  std::shared_ptr<C_BoxCollider> boxCollider = std::dynamic_pointer_cast<C_BoxCollider>(_other);

  if (boxCollider) {
    const sf::FloatRect& rect1 = getCollidable();
    const sf::FloatRect& rect2 = boxCollider->getCollidable();
    if (rect1.intersects(rect2)) {
      Debug::log("Intersection");
      m.colliding = true;
      m.other = &rect2;
    }
  }
  return m;
}

void C_BoxCollider::resolveOverlap(const Manifold& _m) {
  auto transform = owner->transform;

  if (transform->isStatic()) {
    return;
  }

  const sf::FloatRect& rect1 = getCollidable();
  const sf::FloatRect* rect2 = _m.other;

  float resolve = 0;
  float xDiff = (rect1.left + (rect1.width * 0.5f)) - (rect2->left + (rect2->width * 0.5f));
  float yDiff = (rect1.top + (rect1.height * 0.5f)) - (rect2->top + (rect2->height * 0.5f));
  Debug::log("XDiff: " +  std::to_string(xDiff));
  Debug::log("YDiff: " +  std::to_string(yDiff));
  if (fabs(xDiff) > fabs(yDiff)) {
    if (xDiff > 0) {
      resolve = (rect2->left + rect2->width) - rect1.left;
    } else {
      resolve = -((rect1.left + rect1.width) - rect2->left);
    }
    transform->addPosition(resolve, 0);
  } else {
    if (yDiff > 0) {
      resolve = (rect2->top + rect2->height) - rect1.top;
    } else {
      resolve = -((rect1.top + rect1.height) - rect2->top);
    }
    transform->addPosition(0, resolve);
  }
}

void C_BoxCollider::setOffset(const sf::Vector2f& _offset) {
  this->offset = offset;
}

void C_BoxCollider::setOffset(float _x, float _y) {
  offset.x = _x;
  offset.y = _y;
}

void C_BoxCollider::setSize(const sf::Vector2f& _size) {
  AABB.width = _size.x;
  AABB.height = _size.y;
}

void C_BoxCollider::setSize(float _width, float _height) {
  AABB.width = _width;
  AABB.height = _height;
}
