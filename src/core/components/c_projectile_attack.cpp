#include "c_projectile_attack.hpp"
#include "../object.hpp"

std::unordered_map<FacingDirection, sf::IntRect, EnumClassHash> C_ProjectileAttack::textureDirectionBindings = {};
std::unordered_map<FacingDirection, sf::Vector2f, EnumClassHash> C_ProjectileAttack::offsetDirectionBindings = {};
std::unordered_map<FacingDirection, sf::Vector2f, EnumClassHash> C_ProjectileAttack::velocityDirectionBindings = {};


C_ProjectileAttack::C_ProjectileAttack(Object* _owner)
  : Component(_owner)
  , projectileVelocity(400.f) {}

void C_ProjectileAttack::awake() {
  animation = owner->getComponent<C_Animation>();
  direction = owner->getComponent<C_Direction>();
}

void C_ProjectileAttack::update(float _deltaTime) {
  if (owner->context->input->isKeyDown(Input::Key::E)) {
    animation->setAnimationState(AnimationState::Projectile);
  } else if (owner->context->input->isKeyUp(Input::Key::E)) {
    animation->setAnimationState(AnimationState::Idle);
  }
}

void C_ProjectileAttack::start() {
  projectileTextureId = owner->context->textureAllocator->add(owner->context->workingDir->get() + "media/LPC/Weapons/arrow.png");

  if (textureDirectionBindings.size() == 0) {
    textureDirectionBindings.emplace(FacingDirection::Up, sf::IntRect(0, 0, 64, 64));
    textureDirectionBindings.emplace(FacingDirection::Left, sf::IntRect(64, 0, 64, 64));
    textureDirectionBindings.emplace(FacingDirection::Down, sf::IntRect(128, 0, 64, 64));
    textureDirectionBindings.emplace(FacingDirection::Right, sf::IntRect(196, 0, 64, 64));
  }

  if (offsetDirectionBindings.size() == 0) {
    offsetDirectionBindings.emplace(FacingDirection::Up, sf::Vector2f());
    offsetDirectionBindings.emplace(FacingDirection::Left, sf::Vector2f(-8.f, 3.f));
    offsetDirectionBindings.emplace(FacingDirection::Down, sf::Vector2f(-3.f, 15.f));
    offsetDirectionBindings.emplace(FacingDirection::Right, sf::Vector2f(8.f, 3.f));
  }

  if (velocityDirectionBindings.size() == 0) {
    velocityDirectionBindings.emplace(FacingDirection::Up, sf::Vector2f(0.f, -1.f));
    velocityDirectionBindings.emplace(FacingDirection::Left, sf::Vector2f(-1.f, 0.f));
    velocityDirectionBindings.emplace(FacingDirection::Down, sf::Vector2f(0.f, 1.f));
    velocityDirectionBindings.emplace(FacingDirection::Right, sf::Vector2f(1.f, 0.f));
  }

  animation->addAnimationAction(AnimationState::Projectile, FacingDirection::Up, 9, std::bind(&C_ProjectileAttack::spawnProjectile, this));
  animation->addAnimationAction(AnimationState::Projectile, FacingDirection::Left, 9, std::bind(&C_ProjectileAttack::spawnProjectile, this));
  animation->addAnimationAction(AnimationState::Projectile, FacingDirection::Down, 9, std::bind(&C_ProjectileAttack::spawnProjectile, this));
  animation->addAnimationAction(AnimationState::Projectile, FacingDirection::Right, 9, std::bind(&C_ProjectileAttack::spawnProjectile, this));
}

void C_ProjectileAttack::spawnProjectile() {
  FacingDirection faceDir = direction->get();

  std::shared_ptr<Object> projectile = std::make_shared<Object>(owner->context);

  projectile->transform->setPosition(owner->transform->getPosition() + offsetDirectionBindings.at(faceDir));

  auto projSprite = projectile->addComponent<C_Sprite>();
  projSprite->load(projectileTextureId);
  projSprite->setDrawLayer(DrawLayer::Entities);
  projSprite->setSortOrder(100);
  projSprite->setTextureRect(textureDirectionBindings.at(direction->get()));

  auto velocity = projectile->addComponent<C_Velocity>();
  velocity->set(velocityDirectionBindings.at(faceDir) * projectileVelocity);

  auto collider = projectile->addComponent<C_BoxCollider>();
  collider->setSize(32, 32);
  collider->setLayer(CollisionLayer::Projectile);

  projectile->addComponent<C_RemoveObjectOnCollisionEnter>();

  owner->context->objects->add(projectile);
}
