#ifndef c_box_collector_hpp
#define c_box_collector_hpp

#include "../component.hpp"
#include "c_collider.hpp"

class C_BoxCollider : public C_Collider {
public:
  C_BoxCollider(Object* _owner);

  Manifold intersects(std::shared_ptr<C_Collider> _other) override;
  void resolveOverlap(const Manifold& _m) override;

  void setCollidable(const sf::FloatRect& _rect);
  const sf::FloatRect& getCollidable();

  void setOffset(const sf::Vector2f& _offset);
  void setOffset(float _x, float _y);

  void setSize(const sf::Vector2f& _size);
  void setSize(float _width, float _height);

private:
  void setPosition();

  sf::FloatRect AABB;
  sf::Vector2f offset;
};

#endif
