#ifndef c_camera_hpp
#define c_camera_hpp

#include "../component.hpp"

class C_Camera : public Component {
public:
  C_Camera(Object* _owner);

  void lateUpdate(float _deltaTime) override;
};

#endif
