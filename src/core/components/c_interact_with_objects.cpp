#include "c_interact_with_objects.hpp"
#include "../object.hpp"

C_InteractWithObjects::C_InteractWithObjects(Object* owner)
  : Component(owner)
  , interactionDistance(60.f) {}

void C_InteractWithObjects::awake() {
  direction = owner->getComponent<C_Direction>();
}

void C_InteractWithObjects::update(float deltaTime) {
  if (owner->context->input->isKeyDown(Input::Key::R)) {
    // Raycast to find first object in facing direction

    // get direction
    sf::Vector2i heading = direction->getHeading();

    const sf::Vector2f& startPoint = owner->transform->getPosition();

    sf::Vector2f endPoint;
    endPoint.x = startPoint.x + (heading.x * interactionDistance);
    endPoint.y = startPoint.y + (heading.y * interactionDistance);

    RaycastResult result = owner->context->raycast->cast(startPoint, endPoint, owner->instanceId->get());
    if (result.collision != nullptr) {
      auto interactables = result.collision->getComponents<C_Interactable>();
      for (auto& interactable : interactables) {
        interactable->onInteraction(owner);
      }
    }
  }
}