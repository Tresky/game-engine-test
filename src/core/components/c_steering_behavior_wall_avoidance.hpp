#ifndef c_steering_behavior_wall_avoidance_hpp
#define c_steering_behavior_wall_avoidance_hpp

#include "c_steering_behavior.hpp"
#include "c_velocity.hpp"
#include "../raycast.hpp"

class C_SteeringBehaviorWallAvoidance : public C_SteeringBehavior {
public:
  C_SteeringBehaviorWallAvoidance(Object* owner);

  void awake() override;

  const sf::Vector2f getForce() override;

private:
  std::shared_ptr<C_Velocity> velocity;
  float lookDistance;
};

#endif
