#ifndef c_direction_hpp
#define c_direction_hpp

#include "../component.hpp"
#include "../animation.hpp"
#include "c_velocity.hpp"

class C_Direction : public Component {
public:
  C_Direction(Object* _owner);

  void awake() override;

  FacingDirection get();
  sf::Vector2i getHeading();

private:
  std::shared_ptr<C_Velocity> velocity;
  FacingDirection currentDir;
};

#endif
