#include "c_tag.hpp"

C_Tag::C_Tag(Object* owner)
  : Component(owner)
  , tag(Tag::Default) {}

Tag C_Tag::get() const {
  return tag;
}

void C_Tag::set(Tag tag) {
  this->tag = tag;
}

bool C_Tag::compare(std::shared_ptr<C_Tag> other) const {
  return tag == other->tag;
}

bool C_Tag::compare(Tag other) const {
  return tag == other;
}
