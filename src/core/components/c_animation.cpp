#include "c_animation.hpp"
#include "../object.hpp"

C_Animation::C_Animation(Object* _owner)
  : Component(_owner)
  , currentAnimation(AnimationState::None, nullptr)
  , currentDirection(FacingDirection::Down) {}

void C_Animation::awake() {
  sprite = owner->getComponent<C_Sprite>();
  direction = owner->getComponent<C_Direction>();
}

void C_Animation::update(float _deltaTime) {
  setAnimationDirection(direction->get());
  if (currentAnimation.first != AnimationState::None) {
    bool newFrame = currentAnimation.second->updateFrame(_deltaTime);
    if (newFrame) {
      const FrameData& data = *currentAnimation.second->getCurrentFrame();
      sprite->load(data.id);
      sprite->setTextureRect(data.x, data.y, data.width, data.height);
    }
  }
}

void C_Animation::addAnimation(AnimationState _state, AnimationList& _animationList) {
  animations.insert(std::make_pair(_state, _animationList));
  if (currentAnimation.first == AnimationState::None) {
    setAnimationState(_state);
  }
}

void C_Animation::addAnimationAction(AnimationState _state, FacingDirection _dir, int _frame, AnimationAction _action) {
  auto animationList = animations.find(_state);
  if (animationList != animations.end()) {
    auto animation = animationList->second.find(_dir);
    if (animation != animationList->second.end()) {
      animation->second->addFrameAction(_frame, _action);
    }
  }
}

void C_Animation::setAnimationState(AnimationState _state) {
  if (currentAnimation.first == _state) {
    return;
  }

  auto animationList = animations.find(_state);
  if (animationList != animations.end()) {
    auto animation = animationList->second.find(currentDirection);
    if (animation != animationList->second.end()) {
      currentAnimation.first = animationList->first;
      currentAnimation.second = animation->second;
      currentAnimation.second->reset();
    }
  }
}

const AnimationState& C_Animation::getAnimationState() const {
  return currentAnimation.first;
}

void C_Animation::setAnimationDirection(FacingDirection _dir) {
  if (_dir != currentDirection) {
    currentDirection = _dir;

    auto animationList = animations.find(currentAnimation.first);
    if (animationList != animations.end()) {
      auto animation = animationList->second.find(currentDirection);
      if (animation != animationList->second.end()) {
        currentAnimation.second = animation->second;
        currentAnimation.second->reset();
      }
    }
  }
}

