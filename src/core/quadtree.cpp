#include "quadtree.hpp"

Quadtree::Quadtree()
  : Quadtree(5, 5, 0, {0.f, 0.f, 1920, 1080}, nullptr) {}

Quadtree::Quadtree(
  int _maxObjects,
  int _maxLevels,
  int _level,
  sf::FloatRect _bounds,
  Quadtree* _parent
) : maxObjects(_maxObjects)
  , maxLevels(_maxLevels)
  , parent(_parent)
  , level(_level)
  , bounds(_bounds) {}

void Quadtree::insert(std::shared_ptr<C_BoxCollider> _object) {
  if (children[0] != nullptr) {
    // Check among the children of this tree to see where the object belongs
    int indexToPlaceObject = getChildIndexForObject(_object->getCollidable());
    if (indexToPlaceObject != thisTree) {
      // If the child finds a place to go, insert it
      children[indexToPlaceObject]->insert(_object);
      return;
    }
  }

  // If this node has no child nodes, place the object in this node
  objects.emplace_back(_object);

  if (objects.size() > maxObjects && level < maxLevels && children[0] == nullptr) {
    split();

    auto objIterator = objects.begin();
    while (objIterator != objects.end()) {
      auto obj = *objIterator;
      int indexToPlaceObject = getChildIndexForObject(obj->getCollidable());

      if (indexToPlaceObject != thisTree) {
        children[indexToPlaceObject]->insert(obj);
        objIterator = objects.erase(objIterator);
      } else {
        ++objIterator;
      }
    }
  }
}

void Quadtree::remove(std::shared_ptr<C_BoxCollider> _object) {
  int index = getChildIndexForObject(_object->getCollidable());
  if (index == thisTree || children[index] == nullptr) {
    for (int i = 0; i < objects.size(); i++) {
      if (objects.at(i)->owner->instanceId->get() == _object->owner->instanceId->get()) {
        objects.erase(objects.begin() + i);
        break;
      }
    }
  } else {
    return children[index]->remove(_object);
  }
}

void Quadtree::clear() {
  objects.clear();
  for (int i = 0; i < 4; i++) {
    if (children[i] != nullptr) {
      children[i]->clear();
      children[i] = nullptr;
    }
  }
}

std::vector<std::shared_ptr<C_BoxCollider>> Quadtree::search(const sf::FloatRect& _area) {
  std::vector<std::shared_ptr<C_BoxCollider>> possibleOverlaps;
  search(_area, possibleOverlaps);

  std::vector<std::shared_ptr<C_BoxCollider>> returnList;
  for (auto collider : possibleOverlaps) {
    if (_area.intersects(collider->getCollidable())) {
      returnList.emplace_back(collider);
    }
  }
  return returnList;
}

void Quadtree::search(const sf::FloatRect& _area, std::vector<std::shared_ptr<C_BoxCollider>>& _overlappingObjects) {
  _overlappingObjects.insert(_overlappingObjects.end(), objects.begin(), objects.end());

  if (children[0] != nullptr) {
    int index = getChildIndexForObject(_area);
    if (index == thisTree) {
      for (int i = 0; i < 4; i++) {
        if (children[i]->getBounds().intersects(_area)) {
          children[i]->search(_area, _overlappingObjects);
        }
      }
    } else {
      children[index]->search(_area, _overlappingObjects);
    }
  }
}

const sf::FloatRect& Quadtree::getBounds() const {
  return bounds;
}

void Quadtree::drawDebug() {
  if (children[0] != nullptr) {
    for (int i = 0; i < 4; i++) {
      children[i]->drawDebug();
    }
  }
  Debug::drawRect(bounds, sf::Color::Red);
}

int Quadtree::getChildIndexForObject(const sf::FloatRect& _objectBounds) {
  int index = -1;

  double verticalDividingLine = bounds.left + bounds.width * 0.5f;
  double horizontalDividingLine = bounds.top + bounds.height * 0.5f;

  bool north = _objectBounds.top < horizontalDividingLine
    && (_objectBounds.height + _objectBounds.top < horizontalDividingLine);
  bool south = _objectBounds.top > horizontalDividingLine;
  bool west = _objectBounds.left < verticalDividingLine
    && (_objectBounds.left + _objectBounds.width < verticalDividingLine);
  bool east = _objectBounds.left > verticalDividingLine;

  if (east && north) {
    index = childNE;
  } else if (east && south) {
    index = childSE;
  } else if (west && north) {
    index = childNW;
  } else if (west && south) {
    index = childSW;
  }

  return index;
}

void Quadtree::split()
{
  const int childWidth = bounds.width / 2;
  const int childHeight = bounds.height / 2;

  children[childNE] = std::make_shared<Quadtree>(
    maxObjects, maxLevels, level + 1,
    sf::FloatRect(bounds.left + childWidth, bounds.top, childWidth, childHeight), this
  );
  children[childNW] = std::make_shared<Quadtree>(
    maxObjects, maxLevels, level + 1,
    sf::FloatRect(bounds.left, bounds.top, childWidth, childHeight), this
  );
  children[childSW] = std::make_shared<Quadtree>(
    maxObjects, maxLevels, level + 1,
    sf::FloatRect(bounds.left, bounds.top + childHeight, childWidth, childHeight), this
  );
  children[childSE] = std::make_shared<Quadtree>(
    maxObjects, maxLevels, level + 1,
    sf::FloatRect(bounds.left + childWidth, bounds.top + childHeight, childWidth, childHeight), this
  );
}
