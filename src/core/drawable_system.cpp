#include "drawable_system.hpp"

void DrawableSystem::add(std::vector<std::shared_ptr<Object>>& _objects) {
  for (auto o : _objects) {
    add(o);
  }
}

void DrawableSystem::add(std::shared_ptr<Object> _object) {
  std::shared_ptr<C_Drawable> objectsDrawable = _object->getDrawable();
  if (objectsDrawable) {
    DrawLayer layer = objectsDrawable->getDrawLayer();

    auto itr = drawables.find(layer);
    if (itr != drawables.end()) {
      drawables[layer].push_back(objectsDrawable);
    } else {
      std::vector<std::shared_ptr<C_Drawable>> objs;
      objs.push_back(objectsDrawable);
      drawables.insert(std::make_pair(layer, objs));
    }
  }
}

void DrawableSystem::sort() {
  for (auto& layer : drawables) {
    if (!std::is_sorted(layer.second.begin(), layer.second.end(), layerSort)) {
      std::sort(layer.second.begin(), layer.second.end(), layerSort);
    }
  }
}

void DrawableSystem::draw(Window& _window) {
  sort();
  for (auto& layer : drawables) {
    for (auto& drawable : layer.second) {
      drawable->draw(_window);
    }
  }
}

void DrawableSystem::processRemovals() {
  for (auto& layer : drawables) {
    auto objIterator = layer.second.begin();
    while (objIterator != layer.second.end()) {
      auto obj = *objIterator;
      if (!obj->continueToDraw()) {
        Debug::logWarning("Removing from DrawableSystem");
        objIterator = layer.second.erase(objIterator);
      } else {
        ++objIterator;
      }
    }
  }
}

bool DrawableSystem::layerSort(std::shared_ptr<C_Drawable> _a, std::shared_ptr<C_Drawable> _b) {
  return _a->getSortOrder() < _b->getSortOrder();
}
