#include "game.hpp"

Game::Game() : window("game engine test") {
  std::shared_ptr<SceneSplashScreen> splashScreen = 
    std::make_shared<SceneSplashScreen>(workingDir, sceneStateMachine, window, textureAllocator);
  std::shared_ptr<SceneGame> gameScene = std::make_shared<SceneGame>(workingDir, textureAllocator, window, fontAllocator);

  unsigned int splashScreenId = sceneStateMachine.add(splashScreen);
  unsigned int gameSceneId = sceneStateMachine.add(gameScene);

  splashScreen->setSwitchToScene(gameSceneId);
  sceneStateMachine.switchTo(splashScreenId);

  deltaTime = clock.restart().asSeconds();
}

void Game::processInput() {
  sceneStateMachine.processInput();
}

void Game::update() {
  window.update();
  sceneStateMachine.update(deltaTime);
}

void Game::lateUpdate() {
  sceneStateMachine.lateUpdate(deltaTime);
}

void Game::draw() {
  window.beginDraw();

  sceneStateMachine.draw(window);

  window.endDraw();
}

bool Game::isRunning() const {
  return window.isOpen();
}

void Game::calculateDeltaTime() {
  deltaTime = clock.restart().asSeconds();
}