#ifndef working_directory_hpp
#define working_directory_hpp

#include <string>

#ifdef MACOS
#include "CoreFoundation/CoreFoundation.h"
#endif

class WorkingDirectory
{
public:
  WorkingDirectory();

  inline const std::string& get() {
    return path;
  }
private:
  std::string path;
};

#endif
