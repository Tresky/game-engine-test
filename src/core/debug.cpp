#include "debug.hpp"

std::vector<sf::RectangleShape> Debug::rects = {};
std::vector<std::array<sf::Vertex, 2>> Debug::lines = {};

void Debug::draw(Window& _window) {
  for (auto& r : rects) {
    _window.draw(r);
  }
  rects.clear();

  for (auto& l : lines) {
    sf::Vertex line[2] = { l.at(0), l.at(1) };
    _window.draw(line, 2, sf::Lines);
  }
  lines.clear();
}

void Debug::drawRect(const sf::FloatRect& _rect, sf::Color _color, const int _thickness) {
  sf::Vector2f size(_rect.width, _rect.height);
  sf::Vector2f pos(_rect.left, _rect.top);
  sf::RectangleShape shape(size);
  shape.setPosition(pos);
  shape.setOutlineColor(_color);
  shape.setOutlineThickness(_thickness);
  shape.setFillColor(sf::Color::Transparent);
  rects.push_back(shape);
}

void Debug::drawLine(const sf::Vector2f& _from, const sf::Vector2f& _to, sf::Color _color) {
  lines.push_back({ sf::Vertex(_from, _color), sf::Vertex(_to, _color) });
}

void Debug::log(const std::string& _msg) {
  std::cout << _msg << std::endl;
}

void Debug::logWarning(const std::string& _msg) {
  std::cout << "WARNING: " << _msg << std::endl;
}

void Debug::logError(const std::string& _msg) {
  std::cout << "ERROR: " << _msg << std::endl;
}

void Debug::handleCameraZoom(Window& _window, Input& _input) {
  if (_input.isKeyUp(Input::Key::LBracket)) {
    sf::View view = _window.getView();
    view.zoom(1.1f);
    _window.setView(view);
  } else if (_input.isKeyUp(Input::Key::RBracket)) {
    sf::View view = _window.getView();
    view.zoom(0.9f);
    _window.setView(view);
  }
}
