#include <cmath>
#include "raycast.hpp"

Raycast::Raycast(Quadtree& collisions)
  : collisions(collisions) {}

//TODO: using int rather than unsigned int as exclusion id will cause problems if large number of entities.
RaycastResult Raycast::cast(const sf::Vector2f& from, const sf::Vector2f& to, int exclusionId) {
  // Build empty raycast result
  RaycastResult result;
  result.collision = nullptr;
  result.layer = CollisionLayer::Default;

  if (from == to) {
    return result;
  }

  // Create sf::Rect that encompasses line
  sf::FloatRect collisionArea = buildRect(from, to);

  // Debug::drawRect(collisionArea, sf::Color::Red);

  // Retrieve entities from collision system within the raycast area
  std::vector<std::shared_ptr<C_BoxCollider>> entities = collisions.search(collisionArea);

  // If there are no entities in the zone return empty result
  if (entities.size() == 0) {
    return result;
  }

  // Create a stepped line from first to second point
  std::vector<sf::Vector2f> linePoints = buildLinePoints(from, to);

  // for each point in the line check if it intersects with every entity
  for (auto& p : linePoints) {
    for (auto& e : entities) {
      if (exclusionId == e->owner->instanceId->get()) {
        continue;
      }

      sf::FloatRect entityRect = e->getCollidable();
      if (entityRect.contains(p)) {
        result.collision = e->owner;
        result.layer = e->getLayer();
        return result;
      }
    }
  }
  return result;
}

RaycastResult Raycast::cast(const sf::Vector2f& from, const sf::Vector2f& to, CollisionLayer layer) {
  // Build empty raycast result
  RaycastResult result;
  result.collision = nullptr;
  result.layer = CollisionLayer::Default;

  if (from == to) {
    return result;
  }

  // Create sf::Rect that encompasses line
  sf::FloatRect collisionArea = buildRect(from, to);

  // Debug::drawRect(collisionArea, sf::Color::Red);

  // Retrieve entities from collision system within the raycast area
  std::vector<std::shared_ptr<C_BoxCollider>> entities = collisions.search(collisionArea);

  // If there are no entities in the zone return empty result
  if (entities.size() == 0) {
    return result;
  }

  // Create a stepped line from first to second point
  std::vector<sf::Vector2f> linePoints = buildLinePoints(from, to);

  // for each point in the line check if it intersects with every entity
  for (auto& p : linePoints) {
    for (auto& e : entities) {
      if (e->getLayer() != layer) {
        continue;
      }

      sf::FloatRect entityRect = e->getCollidable();
      if (entityRect.contains(p)) {
        result.collision = e->owner;
        result.layer = e->getLayer();
        return result;
      }
    }
  }
  return result;
}

sf::FloatRect Raycast::buildRect(const sf::Vector2f& lineOne, const sf::Vector2f& lineTwo) {
  float left = (lineOne.x < lineTwo.x) ? lineOne.x : lineTwo.x;
  float top = (lineOne.y < lineTwo.y) ? lineOne.y : lineTwo.y;
  float width = fabs(lineOne.x - lineTwo.x);
  float height = fabs(lineOne.y - lineTwo.y);

  return sf::FloatRect(left, top, width, height);
}

//TODO: Look into Bresenham’s Line genration.
std::vector<sf::Vector2f> Raycast::buildLinePoints(const sf::Vector2f& from, const sf::Vector2f& to) {
  // TODO: it would be preferable to calculate in
  // advance the size of "result" and to use a fixed-size array
  // instead of a list.
  std::vector<sf::Vector2f> result;

  sf::Vector2f diff = to - from;
  int steps = 0;

  float absDiffX = abs(diff.x);
  float absDiffY = abs(diff.y);

  if (absDiffX > absDiffY) {
    steps = absDiffX;
  } else {
    steps = absDiffY;
  }

  float xStep = diff.x / (float)steps;
  float yStep = diff.y / (float)steps;

  float newX = from.x;
  float newY = from.y;

  for (int i = 0; i <= steps; i++) {
    result.push_back(sf::Vector2f(newX, newY));
    newX += xStep;
    newY += yStep;
  }

  return result;
}
