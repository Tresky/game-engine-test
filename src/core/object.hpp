#ifndef object_hpp
#define object_hpp

#include "window.hpp"
#include "component.hpp"
#include "shared_context.hpp"
#include "components/c_drawable.hpp"
#include "components/c_transform.hpp"
#include "components/c_instance_id.hpp"
#include "components/c_collidable.hpp"
#include "components/c_tag.hpp"
#include "debug.hpp"

class Object {
public:
  Object(SharedContext* _context);

  // Awake is called whe object created. Use to ensure
  // required components are present.
  void awake();

  // Start is called after awake method. Use to initialize
  // variables.
  void start();

  void update(float _deltaTime);
  void lateUpdate(float _deltaTime);
  void draw(Window& _window);

  bool isQueuedForRemoval();
  void queueForRemoval();

  template <typename T> std::shared_ptr<T> addComponent() {
    static_assert(std::is_base_of<Component, T>::value, "T must derive from Component");
    for (auto& existingComponent : components) {
      if (std::dynamic_pointer_cast<T>(existingComponent)) {
        return std::dynamic_pointer_cast<T>(existingComponent);
      }
    }
    std::shared_ptr<T> newComponent = std::make_shared<T>(this);
    components.push_back(newComponent);

    if (std::dynamic_pointer_cast<C_Drawable>(newComponent)) {
      drawable = std::dynamic_pointer_cast<C_Drawable>(newComponent);
    }

    if (std::dynamic_pointer_cast<C_Collidable>(newComponent)) {
      collidables.push_back(std::dynamic_pointer_cast<C_Collidable>(newComponent));
    }

    return newComponent;
  };

  template <typename T> std::shared_ptr<T> getComponent() {
    for (auto& existingComponent : components) {
      if (std::dynamic_pointer_cast<T>(existingComponent)) {
        return std::dynamic_pointer_cast<T>(existingComponent);
      }
    }
    return nullptr;
  };

  template <typename T> std::vector<std::shared_ptr<T>> getComponents() {
    std::vector<std::shared_ptr<T>> matchingComponents;
    for (auto& existingComponent : components) {
      if (std::dynamic_pointer_cast<T>(existingComponent)) {
        matchingComponents.emplace_back(std::dynamic_pointer_cast<T>(existingComponent));
      }
    }
    return matchingComponents;
  };

  void onCollisionEnter(std::shared_ptr<C_BoxCollider> other);
  void onCollisionStay(std::shared_ptr<C_BoxCollider> other);
  void onCollisionExit(std::shared_ptr<C_BoxCollider> other);

  std::shared_ptr<C_Drawable> getDrawable();

  std::shared_ptr<C_Transform> transform;
  std::shared_ptr<C_InstanceId> instanceId;
  std::shared_ptr<C_Tag> tag;

  SharedContext* context;

private:
  std::vector<std::shared_ptr<Component>> components;
  std::vector<std::shared_ptr<C_Collidable>> collidables;
  std::shared_ptr<C_Drawable> drawable;
  bool queuedForRemoval;
};

#endif
