#ifndef window_hpp
#define window_hpp

#include <SFML/Graphics.hpp>

class Window {
public:
  Window(const std::string& _windowName);

  void update();

  void beginDraw();
  void draw(const sf::Drawable& _drawable);
  void draw(const sf::Vertex* _vertices, std::size_t _vertexCount, sf::PrimitiveType _type);
  void endDraw();

  sf::Vector2f getCenter() const;

  bool isOpen() const;

  const sf::View& getView() const;
  sf::FloatRect getViewSpace() const;
  void setView(const sf::View& _view);

private:
  sf::RenderWindow window;
};

#endif
