#ifndef quadtree_hpp
#define quadtree_hpp

#include <memory>
#include <vector>

#include "components/c_box_collider.hpp"
#include "object.hpp"
#include "debug.hpp"

class Quadtree {
public:
  Quadtree();
  Quadtree(
    int _maxObjects,
    int _maxLevels,
    int _level,
    sf::FloatRect _bounds,
    Quadtree* _parent
  );

  // Insert an object into the quadtree
  void insert(std::shared_ptr<C_BoxCollider> _object);

  // Remove an object from the quadtree when we no longer need it to collide
  void remove(std::shared_ptr<C_BoxCollider> _object);

  // Remove all objects from the quadtree
  void clear();

  // Returns a vector of colliders that intersect with the search area
  std::vector<std::shared_ptr<C_BoxCollider>> search(const sf::FloatRect& _area);

  // Returns the bounds of this node
  const sf::FloatRect& getBounds() const;

  void drawDebug();

private:
  void search(const sf::FloatRect& _area, std::vector<std::shared_ptr<C_BoxCollider>>& _overlappingObjects);

  // Returns the index for the node that will contain the object.
  // -1 is returned if it is this node.
  int getChildIndexForObject(const sf::FloatRect& _objectBounds);

  // Creates the child nodes
  void split();

  static const int thisTree = -1;
  static const int childNE = 0;
  static const int childNW = 1;
  static const int childSW = 2;
  static const int childSE = 3;

  int maxObjects;
  int maxLevels;

  // nullptr if this is the base node
  Quadtree* parent;
  std::shared_ptr<Quadtree> children[4];

  // Stores objects in this node
  std::vector<std::shared_ptr<C_BoxCollider>> objects;

  // How deep the current node is from the base node.
  // The first node starts at 0 and then its child node is at level 1 and so on.
  int level;

  // The bounds of this node
  sf::FloatRect bounds;
};

#endif
