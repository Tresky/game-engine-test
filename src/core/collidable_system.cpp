#include "collidable_system.hpp"

CollidableSystem::CollidableSystem(Quadtree& collisionTree)
  : collisionTree(collisionTree) {
  Bitmask defaultCollisions;
  defaultCollisions.setBit((int)CollisionLayer::Default);
  collisionLayers.insert(
    std::make_pair(CollisionLayer::Default, defaultCollisions)
  );

  collisionLayers.insert(std::make_pair(CollisionLayer::Tile, Bitmask(0)));

  Bitmask playerCollisions;
  playerCollisions.setBit((int)CollisionLayer::Default);
  playerCollisions.setBit((int)CollisionLayer::Tile);
  playerCollisions.setBit((int)CollisionLayer::NPC);
  collisionLayers.insert(std::make_pair(CollisionLayer::Player, playerCollisions));

  Bitmask projectileCollisions;
  projectileCollisions.setBit((int)CollisionLayer::Tile);
  projectileCollisions.setBit((int)CollisionLayer::NPC);
  collisionLayers.insert(std::make_pair(CollisionLayer::Projectile, projectileCollisions));

  Bitmask npcCollisions;
  npcCollisions.setBit((int)CollisionLayer::Tile);
  collisionLayers.insert(std::make_pair(CollisionLayer::NPC, npcCollisions));
}

void CollidableSystem::add(std::vector<std::shared_ptr<Object>>& _objects) {
  for (auto o : _objects) {
    auto collider = o->getComponent<C_BoxCollider>();
    if (collider) {
      // What layer is this object on?
      CollisionLayer layer = collider->getLayer();

      // Is this object's layer already registered?
      auto itr = collidables.find(layer);
      if (itr != collidables.end()) {
        // If the layer exists, just add the collider to it
        collidables[layer].push_back(collider);
      } else {
        // If the layer doesn't exist, add the layer and the collider
        collidables.insert(std::make_pair(layer, std::vector<std::shared_ptr<C_BoxCollider>>{collider}));
      }
    }
  }
}

void CollidableSystem::processRemovals() {
  for (auto& layer : collidables) {
    auto itr = layer.second.begin();
    while (itr != layer.second.end()) {
      if ((*itr)->owner->isQueuedForRemoval()) {
        itr = layer.second.erase(itr);
      } else {
        ++itr;
      }
    }
  }
}

void CollidableSystem::update() {
  // Clear the quadtree and reinsert everything
  // collisionTree.drawDebug();

  processCollidingObjects();

  collisionTree.clear();
  for (auto maps = collidables.begin(); maps != collidables.end(); ++maps) {
    for (auto collidable : maps->second) {
      collisionTree.insert(collidable);
    }
  }
  resolve();
}

void CollidableSystem::resolve() {
  for (auto maps = collidables.begin(); maps != collidables.end(); ++maps) {
    // If this layer collides with nothing then no need to perform and further checks.
    if (collisionLayers[maps->first].getMask() == 0) {
      continue;
    }

    for (auto collidable : maps->second) {
      // If this collidable is static then no need to check if it's colliding with other objects
      if ( collidable->owner->transform->isStatic()) {
        continue;
      }

      std::vector<std::shared_ptr<C_BoxCollider>> collisions = collisionTree.search(collidable->getCollidable());
      for (auto collision : collisions) {
        // Make sure we do not resolve collisions between the same object
        if (collidable->owner->instanceId->get() == collision->owner->instanceId->get()) {
          continue;
        }

        bool layersCollide = collisionLayers[collidable->getLayer()].getBit((int)collision->getLayer());
        if (layersCollide) {
          Manifold m = collidable->intersects(collision);
          if (m.colliding) {
            auto collisionPair = objectsColliding.emplace(std::make_pair(collidable, collision));
            if (collisionPair.second) {
              collidable->owner->onCollisionEnter(collision);
              collision->owner->onCollisionEnter(collidable);
            }

            // Debug::drawRect(collision->getCollidable(), sf::Color::Red);
            // Debug::drawRect(collidable->getCollidable(), sf::Color::Red);

            if(collision->owner->transform->isStatic()) {
              collidable->resolveOverlap(m);
            } else {
              // TODO how to handle collisions when both objects are not static?
              collidable->resolveOverlap(m);
            }
          }
        }
      }
    }
  }
}

void CollidableSystem::processCollidingObjects() {
  auto itr = objectsColliding.begin();
  while (itr != objectsColliding.end()) {
    auto pair = *itr;

    std::shared_ptr<C_BoxCollider> first = pair.first;
    std::shared_ptr<C_BoxCollider> second = pair.second;

    if (first->owner->isQueuedForRemoval() || second->owner->isQueuedForRemoval()) {
      first->owner->onCollisionExit(second);
      second->owner->onCollisionExit(first);
      itr = objectsColliding.erase(itr);
    } else {
      Manifold m = first->intersects(second);
      if (!m.colliding) {
        first->owner->onCollisionExit(second);
        second->owner->onCollisionExit(first);
        itr = objectsColliding.erase(itr);
      } else {
        first->owner->onCollisionStay(second);
        second->owner->onCollisionStay(first);
        ++itr;
      }
    }
  }
}
