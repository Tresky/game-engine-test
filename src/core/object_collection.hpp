#ifndef object_collection_hpp
#define object_collection_hpp

#include <memory>
#include <vector>

#include "object.hpp"
#include "drawable_system.hpp"
#include "collidable_system.hpp"
#include "debug.hpp"

class ObjectCollection {
public:
  ObjectCollection(DrawableSystem& drawableSystem, CollidableSystem& collidableSystem);

  void add(std::shared_ptr<Object> _object);
  void add(std::vector<std::shared_ptr<Object>>& _otherObjects);

  void update(float _deltaTime);
  void lateUpdate(float _deltaTime);
  void draw(Window& _window);

  void processNewObjects();
  void processRemovals();

private:
  std::vector<std::shared_ptr<Object>> objects;
  std::vector<std::shared_ptr<Object>> newObjects;

  DrawableSystem& drawables;
  CollidableSystem& collidables;
};

#endif
