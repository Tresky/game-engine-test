#ifndef shared_context_hpp
#define shared_context_hpp

#include "input.hpp"
#include "working_directory.hpp"
#include "resource_allocator.hpp"
#include "window.hpp"

class ObjectCollection;
class Raycast;
class Quadtree;

struct SharedContext {
  Input* input;
  ObjectCollection* objects;
  WorkingDirectory* workingDir;
  ResourceAllocator<sf::Texture>* textureAllocator;
  ResourceAllocator<sf::Font>* fontAllocator;
  Window* window;
  Raycast* raycast;
  Quadtree* collisionTree;
};

#endif
