#ifndef collidable_system_hpp
#define collidable_system_hpp

#include <vector>
#include <memory>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "enum_class_hash.hpp"
#include "component_pair_hash.hpp"
#include "object.hpp"
#include "quadtree.hpp"
#include "bitmask.hpp"
#include "debug.hpp"

class CollidableSystem {
public:
  CollidableSystem(Quadtree& collisionTree);

  void add(std::vector<std::shared_ptr<Object>>& _objects);
  void processRemovals();
  void update();

private:
  void resolve();
  void processCollidingObjects();

  std::unordered_map<CollisionLayer, Bitmask, EnumClassHash> collisionLayers;
  std::unordered_map<CollisionLayer, std::vector<std::shared_ptr<C_BoxCollider>>, EnumClassHash> collidables;
  std::unordered_set<std::pair<std::shared_ptr<C_BoxCollider>, std::shared_ptr<C_BoxCollider>>, ComponentPairHash> objectsColliding;

  Quadtree& collisionTree;
};

#endif
