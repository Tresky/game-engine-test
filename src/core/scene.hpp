#ifndef scene_hpp
#define scene_hpp

#include "window.hpp"

class Scene {
public:
  virtual ~Scene() {};

  // Called when scene initially created. Called once.
  virtual void onCreate() = 0; 

  // Called when scene destroyed. Called at most once (if a scene 
  // is not removed from the game, this will never be called).
  virtual void onDestroy() = 0; 

  // Called whenever a scene is transitioned into. Can be 
  // called many times in a typical game cycle.
  virtual void onActivate() {}; 

  // Called whenever a transition out of a scene occurs. 
  // Can be called many times in a typical game cycle.
  virtual void onDeactivate() {};

  // The below functions can be overridden as necessary in our scenes.
  virtual void processInput() {};
  virtual void update(float _deltaTime) {};
  virtual void lateUpdate(float _deltaTime) {};
  virtual void draw(Window& _window) {};
};


#endif
