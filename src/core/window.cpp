#include "window.hpp"

Window::Window(const std::string& _windowName)
  : window(sf::VideoMode(1920, 1080), _windowName) {
  window.setVerticalSyncEnabled(true);
  window.setFramerateLimit(60);
}

void Window::update() {
  sf::Event event;
  if (window.pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      window.close();
    }
  }
}

void Window::beginDraw() {
  window.clear(sf::Color::White);
}

void Window::draw(const sf::Drawable& _drawable) {
  window.draw(_drawable);
}

void Window::draw(const sf::Vertex* _vertices, std::size_t _vertexCount, sf::PrimitiveType _type) {
  window.draw(_vertices, _vertexCount, _type);
}

void Window::endDraw() {
  window.display();
}

sf::Vector2f Window::getCenter() const {
  return window.getView().getCenter();
}

bool Window::isOpen() const {
  return window.isOpen();
}

const sf::View& Window::getView() const {
  return window.getView();
}

void Window::setView(const sf::View& _view) {
  window.setView(_view);
}

sf::FloatRect Window::getViewSpace() const {
  const sf::View& view = getView();
  const sf::Vector2f& viewCenter = view.getCenter();
  const sf::Vector2f& viewSize = view.getSize();
  sf::Vector2f viewSizeHalf(viewSize.x * 0.5f, viewSize.y * 0.5f);
  sf::FloatRect viewSpace(viewCenter - viewSizeHalf, viewSize);
  return viewSpace;
}
