#include "animation.hpp"

Animation::Animation()
  : frames(0)
  , currentFrameIndex(0)
  , currentFrameTime(0.f)
  , releaseFirstFrame(true)
  , animIsLooped(true) {}

void Animation::addFrame(int _textureId, int _x, int _y, int _width, int _height, float _frameTime) {
  FrameData data;
  data.id = _textureId;
  data.x = _x;
  data.y = _y;
  data.width = _width;
  data.height = _height;
  data.displayTimeSeconds = _frameTime;
  frames.push_back(data);
}

void Animation::addFrameAction(unsigned int _frame, AnimationAction _action) {
  if (_frame < frames.size()) {
    auto actionKey = actions.find(_frame);
    if (actionKey == actions.end()) {
      framesWithActions.setBit(_frame);
      actions.insert(
        std::make_pair(_frame, std::vector<AnimationAction>{_action})
      );
    } else {
      actionKey->second.emplace_back(_action);
    }
  }
}

const FrameData* Animation::getCurrentFrame() const {
  if (frames.size() > 0) {
    return &frames[currentFrameIndex];
  }
  return nullptr;
}

bool Animation::updateFrame(float _deltaTime) {
  if (releaseFirstFrame) {
    runActionForCurrentFrame();
    releaseFirstFrame = false;
    return true;
  }

  if (frames.size() > 1 && (animIsLooped || currentFrameIndex < frames.size() - 1)) {
    currentFrameTime += _deltaTime;
    if (currentFrameTime >= frames[currentFrameIndex].displayTimeSeconds) {
      currentFrameTime = 0.f;
      incrementFrame();
      runActionForCurrentFrame();
      return true;
    }
  }
  return false;
}

void Animation::incrementFrame() {
  currentFrameIndex = (currentFrameIndex + 1) % frames.size();
}

void Animation::reset() {
  currentFrameIndex = 0;
  currentFrameTime = 0.f;
  releaseFirstFrame = true;
}

void Animation::runActionForCurrentFrame() {
  if (actions.size() > 0) {
    if (framesWithActions.getBit(currentFrameIndex)) {
      auto actionsToRun = actions.at(currentFrameIndex);
      for (auto f : actionsToRun) {
        f();
      }
    }
  }
}

void Animation::setLooped(bool _looped) {
  animIsLooped = _looped;
}

bool Animation::isLooped() {
  return animIsLooped;
}
