#ifndef game_hpp
#define game_hpp

#include "window.hpp"
#include "working_directory.hpp"
#include "input.hpp"
#include "scene_state_machine.hpp"
#include "scenes/scene_splash_screen.hpp"
#include "scenes/scene_game.hpp"

class Game {
public:
  Game();

  void processInput();
  void update();
  void lateUpdate();
  void draw();
  void calculateDeltaTime();
  bool isRunning() const;

private:
  Window window;
  WorkingDirectory workingDir;

  sf::Clock clock;
  float deltaTime;

  SceneStateMachine sceneStateMachine;
  ResourceAllocator<sf::Texture> textureAllocator;
  ResourceAllocator<sf::Font> fontAllocator;
};

#endif
