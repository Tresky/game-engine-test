#ifndef debug_hpp
#define debug_hpp

#include <array>
#include <iostream>
#include <functional>
#include <SFML/Graphics.hpp>

#include "input.hpp"
#include "window.hpp"

class Debug {
public:
  static void draw(Window& _window);

  static void drawRect(const sf::FloatRect& _rect, sf::Color _color = sf::Color::White, const int _thickness = 2);
  static void drawLine(const sf::Vector2f& _from, const sf::Vector2f& _to, sf::Color _color = sf::Color::White);

  static void log(const std::string& _msg);
  static void logWarning(const std::string& _msp);
  static void logError(const std::string& _msg);

  static void handleCameraZoom(Window& _window, Input& _input);

private:
  static std::vector<std::array<sf::Vertex, 2>> lines;
  static std::vector<sf::RectangleShape> rects;
};

#endif
