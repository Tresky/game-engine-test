#ifndef enum_class_hash_hpp
#define enum_class_hash_hpp

#include <cstddef>

struct EnumClassHash {
  template <typename T>
  std::size_t operator()(T t) const {
    return static_cast<std::size_t>(t);
  }
};

#endif