#include "bitmask.hpp"
#include "debug.hpp"

Bitmask::Bitmask() : bits(0) {}

Bitmask::Bitmask(uint32_t _bits) : bits(_bits) {}

void Bitmask::setMask(Bitmask& _other) {
  bits = _other.getMask();
}

uint32_t Bitmask::getMask() const {
  return bits;
}

bool Bitmask::getBit(int _pos) const {
  return (bits & (1 << _pos)) != 0;
}

void Bitmask::setBit(int _pos, bool _on) {
  if(_on) {
    setBit(_pos);
  } else {
    clearBit(_pos);
  }
}

void Bitmask::setBit(int _pos) {
  bits = bits | 1 << _pos;
}

void Bitmask::clearBit(int _pos) {
  bits = bits & ~(1 << _pos);
}

void Bitmask::clear() {
  bits = 0;
}