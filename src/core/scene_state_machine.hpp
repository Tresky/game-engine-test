#ifndef scene_state_machine_hpp
#define scene_state_machine_hpp

#include <memory>
#include <unordered_map>

#include "scene.hpp"
#include "window.hpp"

class SceneStateMachine
{
public:
    SceneStateMachine();

  // ProcessInput, Update, LateUpdate, and Draw will simply be 
  // pass through methods. They will call the correspondingly 
  // named methods of the active scene.
  void processInput();
  void update(float _deltaTime);
  void lateUpdate(float _deltaTime);
  void draw(Window& _window);

  // Adds a scene to the state machine and returns the id of that scene.
  unsigned int add(std::shared_ptr<Scene> scene); 

  // Transitions to scene with specified id.
  void switchTo(unsigned int _id); 
  
  // Removes scene from state machine.
  void remove(unsigned int _id); 

private:
  // Stores all of the scenes associated with this state machine.
    std::unordered_map<unsigned int, std::shared_ptr<Scene>> scenes; 
  
  // Stores a reference to the current scene. Used when drawing/updating.
    std::shared_ptr<Scene> curScene; 
  
  // Stores our current scene id. This is incremented whenever 
  // a scene is added.
    unsigned int insertedSceneId; 
};

#endif