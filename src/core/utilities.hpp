#ifndef utilities_h
#define utilities_h

class Utilities
{
public:
  static inline bool isInteger(const std::string& _s) {
    if (_s.empty() || ((!isdigit(_s[0])) && (_s[0] != '-') && (_s[0] != '+'))) {
      return false;
    }

    char* p;
    strtol(_s.c_str(), &p, 10);
    return (*p == 0);
  }
};

#endif