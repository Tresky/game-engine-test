#include "tilemap_parser.hpp"

TilemapParser::TilemapParser(ResourceAllocator<sf::Texture>& _textureAllocator, SharedContext& _context)
  : textureAllocator(_textureAllocator)
  , context(_context) {}

std::vector<std::shared_ptr<Object>> TilemapParser::parse(const std::string& _file, sf::Vector2i _offset) {
  //TODO: error checking - check file exists before attempting open.
  XMLDocument doc;
  doc.LoadFile(_file.c_str());
  XMLElement* rootNode = doc.FirstChildElement("map");

  // Loads tile layers from XML.
  std::string baseDir = _file.substr(0, _file.find_last_of("/")) + "/";
  Debug::log("Building map tiles");
  std::shared_ptr<MapTiles> map = buildMapTiles(rootNode, baseDir); 
  Debug::log("Finished building map tiles");

  // We need these to calculate the tiles position in world space
  int tileSizeX = rootNode->IntAttribute("tilewidth");
  int tileSizeY = rootNode->IntAttribute("tileheight");

  // This will contain all of our tiles as objects.
  std::vector<std::shared_ptr<Object>> tileObjects;

  unsigned int layerCount = (unsigned int)map->size() - 1;

  // 2
  // We iterate through each layer in the tile map
  for (const auto& layer : *map) {
    // And each tile in the layer
    for (const auto& tile : layer.second->tiles) {
      std::shared_ptr<TileInfo> tileInfo = tile->properties;
      std::shared_ptr<Object> tileObject = std::make_shared<Object>(&context);
           
      //TODO: tile scale should be set at the data level.
      const unsigned int tileScale = 1;

      if (layer.second->isVisible) {
        auto sprite = tileObject->addComponent<C_Sprite>();
        sprite->load(tileInfo->textureId);
        sprite->setTextureRect(tileInfo->textureRect);
        sprite->setScale(tileScale, tileScale);
        sprite->setSortOrder(layerCount);
        sprite->setDrawLayer(DrawLayer::Background);
      }

      // Calculate world position.
      float x = tile->x * (tileSizeX * tileScale);
      float y = tile->y * (tileSizeY * tileScale);
      tileObject->transform->setPosition(x + _offset.x, y + _offset.y);
      tileObject->transform->setStatic(true);

      if (layer.first == "Collisions") {
        auto collider = tileObject->addComponent<C_BoxCollider>();
        float left = x - (tileSizeX * tileScale) * 0.5f;
        float top = y - (tileSizeY * tileScale) * 0.5f;
        float width = tileSizeX * tileScale;
        float height = tileSizeY * tileScale;
        collider->setCollidable(sf::FloatRect(left, top, width, height));
        collider->setLayer(CollisionLayer::Tile);
      }

      // Add new tile Object to the collection.
      tileObjects.emplace_back(tileObject);
    }
    layerCount--;
  }
  return tileObjects;
}

std::shared_ptr<MapTiles> TilemapParser::buildMapTiles(XMLElement* _rootNode, std::string _baseDir) {
  std::shared_ptr<TileSheets> tileSheetData = buildTileSheetData(_rootNode, _baseDir);
  std::shared_ptr<MapTiles> map = std::make_shared<MapTiles>();

  // We loop through each layer in the XML document.
  for (XMLElement* node = _rootNode->LastChildElement("layer"); node; node = node->PreviousSiblingElement("layer")) {
    std::pair<std::string, std::shared_ptr<Layer>> mapLayer = buildLayer(node, tileSheetData);
    Debug::log("Emplacing layer");
    map->emplace_back(mapLayer);
  }

  return map;
}

std::shared_ptr<TileSheets> TilemapParser::buildTileSheetData(XMLElement *_rootNode, std::string _baseDir) {
  TileSheets tileSheets;
  for (XMLElement* tileSheetSourceNode = _rootNode->FirstChildElement("tileset"); tileSheetSourceNode; tileSheetSourceNode = tileSheetSourceNode->NextSiblingElement("tileset")) {
    Debug::log("Building tileset");
    TileSheetData tileSheetData;

    int firstid = tileSheetSourceNode->IntAttribute("firstgid");

    XMLElement* tilesheetNode;
    if (tileSheetSourceNode->Attribute("source") != NULL) {
      // Load tilesheet from another .tsx file
      std::string tilesetXmlFile = _baseDir + tileSheetSourceNode->Attribute("source");
      XMLDocument tilesetDoc;
      tilesetDoc.LoadFile(tilesetXmlFile.c_str());
      tilesheetNode = tilesetDoc.FirstChildElement("tileset");
    } else {
      // Tileset is inline in the .tmx file as just an image
      tilesheetNode = tileSheetSourceNode;
    }

    // Build the tile set data.
    tileSheetData.tileSize.x = tilesheetNode->IntAttribute("tilewidth");
    tileSheetData.tileSize.y = tilesheetNode->IntAttribute("tileheight");

    int tileCount = tilesheetNode->IntAttribute("tilecount");
    tileSheetData.columns = tilesheetNode->IntAttribute("columns");
    tileSheetData.rows = tileCount / tileSheetData.columns;

    XMLElement* imageNode = tilesheetNode->FirstChildElement("image");
    std::string sourcePath = _baseDir + std::string(imageNode->Attribute("source"));
    tileSheetData.textureId = textureAllocator.add(sourcePath);

    //TODO: add error checking - we want to output a message if the texture is not found.
    tileSheetData.imageSize.x = imageNode->IntAttribute("width");
    tileSheetData.imageSize.y = imageNode->IntAttribute("height");

    tileSheets.insert(
      std::make_pair(firstid, std::make_shared<TileSheetData>(tileSheetData))
    );
  }
  return std::make_shared<TileSheets>(tileSheets);
}

std::pair<std::string, std::shared_ptr<Layer>> TilemapParser::buildLayer(XMLElement* _layerNode, std::shared_ptr<TileSheets> _tileSheets) {
  Tileset tileset;
  std::shared_ptr<Layer> layer = std::make_shared<Layer>();

  int width = _layerNode->IntAttribute("width");

  XMLElement* dataNode = _layerNode->FirstChildElement("data");
  const char* mapIndices = dataNode->GetText();

  std::stringstream fileStream(mapIndices);

  int count = 0;

  std::string line;
  while (fileStream.good()) {
    std::string substr;
    std::getline(fileStream, substr, ',');

    if (!Utilities::isInteger(substr)) {
      // We remove special characters from the int before parsing
      substr.erase(std::remove(substr.begin(), substr.end(), '\r'), substr.end()); 
      substr.erase(std::remove(substr.begin(), substr.end(), '\n'), substr.end());
      //TODO: add additional check to confirm that the character removals have worked:
    }

    int tileId = std::stoi(substr);
    if (tileId != 0) {
      auto itr = tileset.find(tileId);
      if (itr == tileset.end()) {

        int firstId = 0;
        std::shared_ptr<TileSheetData> tileSheet;
        for (auto iter = _tileSheets->rbegin(); iter != _tileSheets->rend(); ++iter) {
          if (tileId >= iter->first) {
            firstId = iter->first;
            tileSheet = iter->second;
            break;
          }
        }

        if (!tileSheet) {
          continue;
        }


        int textureX = (tileId - firstId) % tileSheet->columns;
        int textureY = (tileId - firstId) / tileSheet->columns;

        std::shared_ptr<TileInfo> tileInfo = std::make_shared<TileInfo>(
          tileSheet->textureId, tileId, 
          sf::IntRect(
            textureX * tileSheet->tileSize.x, 
            textureY * tileSheet->tileSize.y, 
            tileSheet->tileSize.x, 
            tileSheet->tileSize.y
          )
        );

        itr = tileset.insert(std::make_pair(tileId, tileInfo)).first;
      }

      std::shared_ptr<Tile> tile = std::make_shared<Tile>();
      // Bind properties of a tile from a set.
      tile->properties = itr->second;
      tile->x = count % width;
      tile->y = count / width;

      layer->tiles.emplace_back(tile);
    }
    count++;
  }

  const std::string layerName = _layerNode->Attribute("name");

  bool layerVisible = true;
  if (_layerNode->Attribute("visible")) {
    layerVisible = std::stoi(_layerNode->Attribute("visible"));
  }
  layer->isVisible = layerVisible;

  return std::make_pair(layerName, layer);
}
