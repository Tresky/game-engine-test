#ifndef tilemap_parser_hpp
#define tilemap_parser_hpp

#include <SFML/Graphics.hpp>
#include <unordered_map>
#include <sstream>
#include <string>

// #include "../../rapidxml/rapidxml.hpp"
// #include "../../rapidxml/rapidxml_utils.hpp"
#include "../../tinyxml/tinyxml2.h"
#include "../utilities.hpp"
#include "../object.hpp"
#include "../working_directory.hpp"
#include "../components/c_sprite.hpp"
#include "../components/c_box_collider.hpp"
#include "tile.hpp"

using namespace tinyxml2;

struct TileSheetData {
  int textureId;
  sf::Vector2u imageSize;
  int columns;
  int rows;
  sf::Vector2u tileSize;
};

struct Layer {
  std::vector<std::shared_ptr<Tile>> tiles;
  bool isVisible;
};

using MapTiles = std::vector<std::pair<std::string, std::shared_ptr<Layer>>>;
using Tileset = std::unordered_map<unsigned int, std::shared_ptr<TileInfo>>;
using TileSheets = std::map<int, std::shared_ptr<TileSheetData>>;

class TilemapParser {
public:
  TilemapParser(ResourceAllocator<sf::Texture>& _textureAllocator, SharedContext& _context);

  std::vector<std::shared_ptr<Object>> parse(const std::string& _file, sf::Vector2i _offset);

private:
  std::shared_ptr<TileSheets> buildTileSheetData(XMLElement* _rootNode, std::string _baseDir);
  std::shared_ptr<MapTiles> buildMapTiles(XMLElement* _rootNode, std::string _baseDir);
  std::pair<std::string, std::shared_ptr<Layer>> buildLayer(XMLElement* _layerNode, std::shared_ptr<TileSheets> _tileSheets);

  ResourceAllocator<sf::Texture>& textureAllocator;
  std::string baseDir;

  SharedContext& context;
};

#endif
