#ifndef tile_hpp
#define tile_hpp

#include "../resource_allocator.hpp"

struct TileInfo {
  TileInfo() : tileId(-1) {}

  TileInfo(int _textureId, unsigned int _tileId, sf::IntRect _textureRect)
    : tileId(_tileId), textureId(_textureId), textureRect(_textureRect) {}

  int tileId;
  int textureId;
  sf::IntRect textureRect;
};

struct Tile {
  std::shared_ptr<TileInfo> properties;
  int x;
  int y;
};

#endif
