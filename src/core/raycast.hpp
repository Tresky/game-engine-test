#ifndef raycast_hpp
#define raycast_hpp

#include "object.hpp"
#include "collidable_system.hpp"

struct RaycastResult {
  Object* collision;
  CollisionLayer layer;
};

class Raycast {
public:
  Raycast(Quadtree& collisions);

  RaycastResult cast(const sf::Vector2f& from, const sf::Vector2f& to, int exclusionId = -1);
  RaycastResult cast(const sf::Vector2f& from, const sf::Vector2f& to, CollisionLayer layer);

private:
  sf::FloatRect buildRect(const sf::Vector2f& lineOne, const sf::Vector2f& lineTwo);
  std::vector<sf::Vector2f> buildLinePoints(const sf::Vector2f& from, const sf::Vector2f& to);

  Quadtree& collisions;
};

#endif
