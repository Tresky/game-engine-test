#ifndef component_pair_hash_hpp
#define component_pair_hash_hpp

#include <cstddef>

struct ComponentPairHash {
  template <typename T>
  std::size_t operator()(T t) const {
    std::size_t x = t.first->owner->instanceId->get();
    std::size_t y = t.first->owner->instanceId->get();
    return (x >= y) ? (x * x + x + y) : (y * y + y + x);
  }
};

#endif