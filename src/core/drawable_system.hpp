#ifndef drawable_system_hpp
#define drawable_system_hpp

#include <map>

#include "components/c_drawable.hpp"
#include "object.hpp"
#include "debug.hpp"

class DrawableSystem {
public:
  void add(std::vector<std::shared_ptr<Object>>& _object);

  void processRemovals();

  void draw(Window& _window);

private:
  void add(std::shared_ptr<Object> _object);
  void sort();

  static bool layerSort(std::shared_ptr<C_Drawable> _a, std::shared_ptr<C_Drawable> _b);

  std::map<DrawLayer, std::vector<std::shared_ptr<C_Drawable>>> drawables;
};

#endif
