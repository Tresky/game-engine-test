#ifndef animation_hpp
#define animation_hpp

#include <vector>
#include <functional>
#include <map>

#include "bitmask.hpp"

struct FrameData {
  int id;
  int x;
  int y;
  int width;
  int height;
  float displayTimeSeconds;
};

enum class FacingDirection {
  None,
  Left,
  Right,
  Up,
  Down
};

using AnimationAction = std::function<void(void)>;

class Animation {
public:
  Animation();

  void addFrame(
    int _textureId,
    int _x,
    int _y,
    int _width,
    int _height,
    float _frameTime
  );

  void addFrameAction(unsigned int _frame, AnimationAction _action);

  const FrameData* getCurrentFrame() const;
  bool updateFrame(float _deltaTime);
  void reset();

  void setLooped(bool _looped);
  bool isLooped();

private:
  void incrementFrame();
  void runActionForCurrentFrame();

  std::vector<FrameData> frames;
  int currentFrameIndex;
  float currentFrameTime;
  bool releaseFirstFrame;
  bool animIsLooped;

  Bitmask framesWithActions;

  std::map<int, std::vector<AnimationAction>> actions;
};

#endif
