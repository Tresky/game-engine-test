#include "scene_state_machine.hpp"

SceneStateMachine::SceneStateMachine()
  : scenes(0)
  , curScene(0)
  , insertedSceneId(0) {}

void SceneStateMachine::processInput() {
    if (curScene) {
      curScene->processInput();
    }
}

void SceneStateMachine::update(float _deltaTime)
{
  if (curScene) {
    curScene->update(_deltaTime);
  }
}

void SceneStateMachine::lateUpdate(float _deltaTime)
{
  if (curScene) {
    curScene->lateUpdate(_deltaTime);
  }
}

void SceneStateMachine::draw(Window& _window) {
    if (curScene) {
      curScene->draw(_window);
    }
}

unsigned int SceneStateMachine::add(std::shared_ptr<Scene> scene) {
  auto inserted = scenes.insert(std::make_pair(insertedSceneId, scene));
  if (inserted.second) {
    scene->onCreate();
    insertedSceneId++;
  }
  return inserted.first->first;
}

void SceneStateMachine::remove(unsigned int _id) {
  auto it = scenes.find(_id);
  if (it != scenes.end()) {
    if (curScene == it->second) {
      curScene = nullptr;
    }
    it->second->onDestroy();
    scenes.erase(it);
  }
}

void SceneStateMachine::switchTo(unsigned int _id) {
  auto it = scenes.find(_id);
  if (it != scenes.end()) {
    if (curScene) {
      curScene->onDeactivate();
    }
    curScene = it->second;
    curScene->onActivate();
  }
}
