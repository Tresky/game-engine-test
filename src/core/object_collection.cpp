#include "object_collection.hpp"

ObjectCollection::ObjectCollection(DrawableSystem& drawableSystem, CollidableSystem& collidableSystem)
  : drawables(drawableSystem)
  , collidables(collidableSystem) {}

void ObjectCollection::add(std::shared_ptr<Object> _object) {
  Debug::log(std::to_string(newObjects.size()));
  newObjects.push_back(_object);
}

void ObjectCollection::add(std::vector<std::shared_ptr<Object>>& _otherObjects) {
  newObjects.insert(newObjects.end(), _otherObjects.begin(), _otherObjects.end());
}

void ObjectCollection::update(float _deltaTime) {
  for (auto& o : objects) {
    o->update(_deltaTime);
  }
  collidables.update();
}

void ObjectCollection::lateUpdate(float _deltaTime) {
  for (auto& o : objects) {
    o->lateUpdate(_deltaTime);
  }
}

void ObjectCollection::draw(Window& _window) {
  drawables.draw(_window);
}

void ObjectCollection::processNewObjects() {
  if (newObjects.size() > 0) {
    for (const auto& o : newObjects) {
      o->awake();
    }
    for (const auto& o : newObjects) {
      o->start();
    }
    objects.insert(objects.end(), newObjects.begin(), newObjects.end());
    drawables.add(newObjects);
    collidables.add(newObjects);
    newObjects.clear();
  }
}

void ObjectCollection::processRemovals() {
  bool removed = false;
  auto objIterator = objects.begin();
  while (objIterator != objects.end()) {
    auto obj = *objIterator;
    if (obj->isQueuedForRemoval()) {
      Debug::logWarning("Removing from ObjectCollection");
      objIterator = objects.erase(objIterator);
      removed = true;
    } else {
      ++objIterator;
    }
  }
  if (removed) {
    drawables.processRemovals();
    collidables.processRemovals();
  }
}
