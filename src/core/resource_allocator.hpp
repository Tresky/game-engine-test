#ifndef resource_allocator_hpp
#define resource_allocator_hpp

#include <map>
#include <memory>
#include <string>

template<typename T>
class ResourceAllocator {
public:
  int add(const std::string& _filePath) {
    auto it = resources.find(_filePath);
    if (it != resources.end()) {
      return it->second.first;
    }

    std::shared_ptr<T> resource = std::make_shared<T>();
    if (!resource->loadFromFile(_filePath)) {
      return -1;
    }

    resources.insert(std::make_pair(_filePath, std::make_pair(currentId, resource)));
    return currentId++;
  }

  void remove(int _id) {
    for (auto it = resources.begin(); it != resources.end(); ++it) {
      if (it->second.first == _id) {
        resources.erase(it->first);
      }
    }
  }

  std::shared_ptr<T> get(int _id) {
    for (auto it = resources.begin(); it != resources.end(); ++it) {
      if (it->second.first == _id) {
        return it->second.second;
      }
    }
    return nullptr;
  }

  bool has(int _id) {
    return (get(_id) != nullptr);
  }

private:
  int currentId;
  std::map<std::string, std::pair<int, std::shared_ptr<T>>> resources;
};

#endif
