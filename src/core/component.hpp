#ifndef component_hpp
#define component_hpp

#include "window.hpp"

class Object;

class Component {
public:
  Component(Object* _owner) : owner(_owner) {};

  virtual ~Component() = default;

  virtual void awake() {};
  virtual void start() {};

  virtual void update(float _deltaTime) {};
  virtual void lateUpdate(float _deltaTime) {};

  Object* owner;
};

#endif
