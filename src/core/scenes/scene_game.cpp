#include "scene_game.hpp"
#include "../object.hpp"

SceneGame::SceneGame(WorkingDirectory& workingDir, ResourceAllocator<sf::Texture>& textureAllocator, Window& window, ResourceAllocator<sf::Font>& fontAllocator)
  : window(window)
  , workingDir(workingDir)
  , textureAllocator(textureAllocator)
  , fontAllocator(fontAllocator)
  , objects(drawableSystem, collisionSystem)
  , mapParser(textureAllocator, context)
  , collisionSystem(collisionTree)
  , collisionTree(5, 5, 0, {-1216, -16, 2560, 1600}, nullptr)
  , raycast(collisionTree) {}

void SceneGame::onCreate() {
  context.input = &input;
  context.objects = &objects;
  context.workingDir = &workingDir;
  context.textureAllocator = &textureAllocator;
  context.window = &window;
  context.raycast = &raycast;
  context.fontAllocator = &fontAllocator;
  context.collisionTree = &collisionTree;

  createPlayer();
  createFoe();
  // createFriend();

  // sf::Vector2i mapOffset(0, 180);
  // std::cout << "Loading tilemap" << workingDir.get() + "media/City.tmx" << std::endl;
  // std::vector<std::shared_ptr<Object>> levelTiles = mapParser.parse(workingDir.get() + "media/House Exterior.tmx", mapOffset);
  sf::Vector2i mapOffset(-1200, 0);
  std::cout << "Loading tilemap" << workingDir.get() + "media/Dungeon.tmx" << std::endl;
  std::vector<std::shared_ptr<Object>> levelTiles = mapParser.parse(workingDir.get() + "media/Dungeon.tmx", mapOffset);
  objects.add(levelTiles);
}

void SceneGame::onDestroy() {}

void SceneGame::processInput() {
  input.update();
}

void SceneGame::update(float _deltaTime) {
  objects.processRemovals();
  objects.processNewObjects();
  objects.update(_deltaTime);

  Debug::handleCameraZoom(window, input);
}

void SceneGame::lateUpdate(float _deltaTime) {
  objects.lateUpdate(_deltaTime);
}

void SceneGame::draw(Window& _window) {
  objects.draw(_window);
  Debug::draw(_window);
}

void SceneGame::createPlayer() {
  std::shared_ptr<Object> player = std::make_shared<Object>(&context);

  player->transform->setPosition(100, 700);

  auto sprite = player->addComponent<C_Sprite>();
  sprite->setDrawLayer(DrawLayer::Entities);

  player->addComponent<C_KeyboardMovement>();

  const int textureId = textureAllocator.add(workingDir.get() + "media/Player.png");
  addAnimationComponent(player, textureId);

  auto collider = player->addComponent<C_BoxCollider>();
  collider->setSize(64 * 0.4f, 64 * 0.5f);
  collider->setOffset(0.f, 14.f);
  collider->setLayer(CollisionLayer::Player);

  player->addComponent<C_Camera>();
  player->addComponent<C_ProjectileAttack>();
  player->addComponent<C_Velocity>();
  player->addComponent<C_MovementAnimation>();
  player->addComponent<C_Direction>();
  player->addComponent<C_InteractWithObjects>();

  player->tag->set(Tag::Player);

  objects.add(player);
}

void SceneGame::createFriend() {
  std::shared_ptr<Object> npc = std::make_shared<Object>(&context);

  npc->transform->setPosition(160, 700);

  auto sprite = npc->addComponent<C_Sprite>();
  sprite->setDrawLayer(DrawLayer::Entities);

  const int textureId = textureAllocator.add(workingDir.get() + "media/Skeleton.png");
  addAnimationComponent(npc, textureId);

  auto collider = npc->addComponent<C_BoxCollider>();
  collider->setSize(64 * 0.4f, 64 * 0.5f);
  collider->setOffset(0.f, 14.f);
  collider->setLayer(CollisionLayer::NPC);

  npc->addComponent<C_Velocity>();
  npc->addComponent<C_MovementAnimation>();
  npc->addComponent<C_Direction>();
  npc->addComponent<C_InteractableTalking>();
  npc->addComponent<C_WalkInLine>();

  objects.add(npc);
}

void SceneGame::createFoe() {
  std::shared_ptr<Object> npc = std::make_shared<Object>(&context);

  npc->transform->setPosition(160, 700);

  auto sprite = npc->addComponent<C_Sprite>();
  sprite->setDrawLayer(DrawLayer::Entities);

  const int textureId = textureAllocator.add(workingDir.get() + "media/Orc.png");
  addAnimationComponent(npc, textureId);

  auto collider = npc->addComponent<C_BoxCollider>();
  collider->setSize(64 * 0.4f, 64 * 0.5f);
  collider->setOffset(0.f, 14.f);
  collider->setLayer(CollisionLayer::NPC);

  npc->addComponent<C_Velocity>();
  npc->addComponent<C_MovementAnimation>();
  npc->addComponent<C_Direction>();

  npc->addComponent<C_BehaviorApplier>();
  npc->addComponent<C_SteeringBehaviorWallAvoidance>();
  auto chase = npc->addComponent<C_SteeringBehaviorChase>();
  chase->setTarget(Tag::Player);

  npc->tag->set(Tag::NPC);

  objects.add(npc);
}

void SceneGame::addAnimationComponent(std::shared_ptr<Object> object, const int textureId) {
  auto animation = object->addComponent<C_Animation>();

  const int frameWidth = 64;
  const int frameHeight = 64;

  const FacingDirection directions[4] = {
    FacingDirection::Up, 
    FacingDirection::Left, 
    FacingDirection::Down, 
    FacingDirection::Right
  };

  /*******************
   * Idle Animations *
   *******************/
  const bool idleAnimationLooped = false;
  unsigned int idleYFramePos = 512;
  std::unordered_map<FacingDirection, std::shared_ptr<Animation>, EnumClassHash> idleAnimations;

  for (int i = 0; i < 4; i++) {
    std::shared_ptr<Animation> idleAnimation = std::make_shared<Animation>();
    idleAnimation->setLooped(idleAnimationLooped);
    idleAnimation->addFrame(textureId, 0, idleYFramePos + frameHeight * i, frameWidth, frameHeight, 0.f);
    idleAnimations.insert(std::make_pair(directions[i], idleAnimation));
  }
  animation->addAnimation(AnimationState::Idle, idleAnimations);

  /**********************
   * Walking Animations *
   **********************/
  const bool walkAnimationLooped = true;
  const int walkingFrameCount = 9;
  const float walkingDelay = 0.1f;
  unsigned int walkingYFramePos = 512;

  std::unordered_map<FacingDirection, std::shared_ptr<Animation>, EnumClassHash> walkingAnimations;

  for (int i = 0; i <4; i++) {
    std::shared_ptr<Animation> walkingAnimation = std::make_shared<Animation>();
    walkingAnimation->setLooped(walkAnimationLooped);
    for (int f = 0; f < walkingFrameCount; f++) {
      walkingAnimation->addFrame(textureId, f * frameWidth, walkingYFramePos + i * frameHeight, frameWidth, frameHeight, walkingDelay);
    }
    walkingAnimations.insert(std::make_pair(directions[i], walkingAnimation));
  }
  animation->addAnimation(AnimationState::Walk, walkingAnimations);

  /*************************
   * Projectile Animations *
   *************************/
  const bool projectileAnimationLooped = false;
  const int projectileFrameCount = 10;
  const float projectileDelay = 0.08f;

  std::unordered_map<FacingDirection, std::shared_ptr<Animation>, EnumClassHash> projectileAnimations;

  unsigned int projFrameYPos = 1024;

  for (int i = 0; i < 4; i++) {
    std::shared_ptr<Animation> projAnimation = std::make_shared<Animation>();
    projAnimation->setLooped(projectileAnimationLooped);
    for (int f = 0; f < projectileFrameCount; f++) {
      projAnimation->addFrame(textureId, f * frameWidth, projFrameYPos + i * frameHeight, frameWidth, frameHeight, projectileDelay);
    }
    projectileAnimations.insert(std::make_pair(directions[i], projAnimation));
  }
  animation->addAnimation(AnimationState::Projectile, projectileAnimations);
}