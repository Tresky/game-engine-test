#include "scene_splash_screen.hpp"

SceneSplashScreen::SceneSplashScreen(
  WorkingDirectory& _workingDir,
  SceneStateMachine& _sceneStateMachine,
  Window& _window,
  ResourceAllocator<sf::Texture>& _textureAllocator
) : textureAllocator(_textureAllocator)
  , workingDir(_workingDir)
  , sceneStateMachine(_sceneStateMachine)
  , window(_window)
  , showForSeconds(1.f)
  , currentSeconds(0.f)
  , switchToState(0) {}

void SceneSplashScreen::onCreate() {
  int textureId = textureAllocator.add(workingDir.get() + "splash-screen.png");
  if (textureId >= 0) {
    std::shared_ptr<sf::Texture> texture = textureAllocator.get(textureId);
    splashSprite.setTexture(*texture);

    sf::FloatRect spriteSize = splashSprite.getLocalBounds();
    splashSprite.setOrigin(spriteSize.width * 0.5f, spriteSize.height * 0.5f);
    splashSprite.setScale(1.f, 1.f);

    sf::Vector2f windowCenter = window.getCenter();
    splashSprite.setPosition((unsigned int)windowCenter.x, (unsigned int)windowCenter.y);
  }
}

void SceneSplashScreen::onActivate() {
  currentSeconds = 0.f;
}

void SceneSplashScreen::onDestroy() {}

void SceneSplashScreen::setSwitchToScene(unsigned int _id) {
  switchToState = _id;
}

void SceneSplashScreen::update(float _deltaTime) {
  currentSeconds += _deltaTime;
  if (currentSeconds >= showForSeconds) {
    sceneStateMachine.switchTo(switchToState);
  }
}

void SceneSplashScreen::draw(Window& _window) {
  _window.draw(splashSprite);
}