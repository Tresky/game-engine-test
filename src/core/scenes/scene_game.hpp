#ifndef scene_game_hpp
#define scene_game_hpp

#include <SFML/Graphics.hpp>

#include "../scene.hpp"
#include "../input.hpp"
#include "../working_directory.hpp"
#include "../resource_allocator.hpp"
#include "../object_collection.hpp"
#include "../raycast.hpp"
#include "../shared_context.hpp"
#include "../tilemap/tilemap_parser.hpp"
#include "../components/c_sprite.hpp"
#include "../components/c_animation.hpp"
#include "../components/c_keyboard_movement.hpp"
#include "../components/c_box_collider.hpp"
#include "../components/c_camera.hpp"
#include "../components/c_projectile_attack.hpp"
#include "../components/c_velocity.hpp"
#include "../components/c_movement_animation.hpp"
#include "../components/c_direction.hpp"
#include "../components/c_interact_with_objects.hpp"
#include "../components/c_interactable_talking.hpp"
#include "../components/c_walk_in_line.hpp"
#include "../components/c_behavior_applier.hpp"
#include "../components/c_steering_behavior_chase.hpp"
#include "../components/c_steering_behavior_wall_avoidance.hpp"

#include "../debug.hpp"

class SceneGame : public Scene {
public:
  SceneGame(WorkingDirectory& workingDir, ResourceAllocator<sf::Texture>& textureAllocator, Window& window, ResourceAllocator<sf::Font>& fontAllocator);

  void onCreate() override;
  void onDestroy() override;

  void processInput() override;
  void update(float _deltaTime) override;
  void lateUpdate(float _deltaTime) override;
  void draw(Window& _window) override;

private:
  void createPlayer();
  void createFriend();
  void createFoe();
  void addAnimationComponent(std::shared_ptr<Object> object, const int textureId);

  Window& window;
  WorkingDirectory& workingDir;
  ResourceAllocator<sf::Texture>& textureAllocator;
  ResourceAllocator<sf::Font>& fontAllocator;
  Input input;
  ObjectCollection objects;
  TilemapParser mapParser;
  DrawableSystem drawableSystem;
  CollidableSystem collisionSystem;
  Quadtree collisionTree;
  Raycast raycast;

  SharedContext context;
};

#endif