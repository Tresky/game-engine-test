#ifndef scene_splash_screen_hpp
#define scene_splash_screen_hpp

#include <SFML/Graphics.hpp>

#include "../scene.hpp"
#include "../scene_state_machine.hpp"
#include "../working_directory.hpp"
#include "../resource_allocator.hpp"

class SceneSplashScreen : public Scene {
public:
  SceneSplashScreen(
    WorkingDirectory& _workingDir,
    SceneStateMachine& _sceneStateMachine,
    Window& _window,
    ResourceAllocator<sf::Texture>& _textureAllocator
  );

  void onCreate() override;
  void onDestroy() override;

  void onActivate() override;

  void setSwitchToScene(unsigned int _id);

  void update(float _deltaTime) override;
  void draw(Window& _window) override;

private:
  ResourceAllocator<sf::Texture>& textureAllocator;
  sf::Sprite splashSprite;

  WorkingDirectory& workingDir;
  SceneStateMachine& sceneStateMachine;
  Window& window;

  // We want to show this scene for a set amount of time
  float showForSeconds;

  // How long the scene has currently been visible.
  float currentSeconds;

  // The state we want to transition to when this scenes time expires.
  unsigned int switchToState;
};

#endif