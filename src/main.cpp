#include "./core/game.hpp"

int main () {
  Game game;

  while (game.isRunning()) {
    game.processInput();
    game.update();
    game.lateUpdate();
    game.draw();
    game.calculateDeltaTime();
  }

  return 0;
}
