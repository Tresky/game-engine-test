# Compiler and flags
CXX = g++
CXXFLAGS = -std=c++11 -pedantic -Wall -ggdb

# Relative directories
ODIR = obj

# Libraries to link to
LIBS = -lsfml-graphics -lsfml-window -lsfml-system #-llua -L/usr/local/lib/lua
# INCS = -I/usr/local/include/lua


#### OBJECT FILES ####
# Engine Objects
# _ENG_OBJ = system.o mode_manager.o video_manager.o script.o script_read.o resources.o image.o input.o lua_bindings.o
# ENG_OBJ = $(patsubst %, $(ODIR)/%, $(_ENG_OBJ))
_ENG_OBJ = game.o window.o working_directory.o bitmask.o input.o scene_state_machine.o scene_splash_screen.o scene_game.o object.o object_collection.o animation.o drawable_system.o quadtree.o collidable_system.o debug.o raycast.o
ENG_OBJ = $(patsubst %, $(ODIR)/%, $(_ENG_OBJ))

_SCENE_OBJ = scene_splash_screen.o scene_game.o
SCENE_OBJ = $(patsubst %, $(ODIR)/%, $(_SCENE_OBJ))

_COMP_OBJ = c_sprite.o c_transform.o c_keyboard_movement.o c_animation.o c_drawable.o c_drawable.o c_collider.o c_box_collider.o c_instance_id.o c_camera.o c_projectile_attack.o c_velocity.o c_movement_animation.o c_direction.o c_remove_object_on_collision_enter.o c_interact_with_objects.o c_interactable_talking.o c_ui_world_label.o c_walk_in_line.o c_tag.o c_steering_behavior.o c_behavior_applier.o c_steering_behavior_chase.o c_steering_behavior_wall_avoidance.o
COMP_OBJ = $(patsubst %, $(ODIR)/%, $(_COMP_OBJ))

_TILE_OBJ = tilemap_parser.o
TILE_OBJ = $(patsubst %, $(ODIR)/%, $(_TILE_OBJ))

_EXT_OBJ = tinyxml2.o
EXT_OBJ = $(patsubst %, $(ODIR)/%, $(_EXT_OBJ))

# Video Utils
# _VUTIL_OBJ = fade.o
# VUTIL_OBJ = $(patsubst %, $(ODIR)/%, $(_VUTIL_OBJ))

# # Global Objects
# _GLOB_OBJ = global.o global_actors.o
# GLOB_OBJ = $(patsubst %, $(ODIR)/%, $(_GLOB_OBJ))

# # Mode Objects
# _MOD_OBJ = map_utils.o map_mode.o map_tiles.o map_objects.o map_sprites.o map_events.o map_zones.o map_combat.o
# MOD_OBJ = $(patsubst %, $(ODIR)/%, $(_MOD_OBJ))



#### DEPENDENCY HEADER FILES ####
# Engine Headers
# _ENG_DEP = system.hpp mode_manager.hpp video_manager.hpp script.hpp script_read.hpp resources.hpp image.hpp input.hpp
# ENG_DEP = $(patsubst %, ./src/core/%, $(_ENG_DEP))
_ENG_DEP = game.hpp window.hpp working_directory.hpp bitmask.hpp input.hpp scene.hpp scene_state_machine.hpp object.hpp component.hpp resource_allocator.hpp object_collection.hpp animation.hpp drawable_system.hpp quadtree.hpp collidable_system.hpp debug.hpp raycast.hpp
ENG_DEP = $(patsubst %, ./src/core/%, $(_ENG_DEP))

_SCENE_DEP = scene_splash_screen.hpp scene_game.hpp
SCENE_DEP = $(patsubst %, ./src/core/scenes/%, $(_SCENE_DEP))

_COMP_DEP = c_sprite.hpp c_transform.hpp c_keyboard_movement.hpp c_animation.hpp c_drawable.hpp c_collider.hpp c_box_collider.hpp c_instance_id.hpp c_camera.hpp c_projectile_attack.hpp c_velocity.hpp c_movement_animation.hpp c_direction.hpp c_remove_object_on_collision_enter.hpp c_interactable.hpp c_interact_with_objects.hpp c_interactable_talking.hpp c_ui_world_label.hpp c_walk_in_line.hpp c_tag.hpp c_steering_behavior.hpp c_behavior_applier.hpp c_steering_behavior_chase.hpp c_steering_behavior_wall_avoidance.hpp
COMP_DEP = $(patsubst %, ./src/core/components/%, $(_COMP_DEP))

_TILE_DEP = tilemap_parser.hpp
TILE_DEP = $(patsubst %, ./src/core/tilemap/%, $(_TILE_DEP))

_EXT_DEP = tinyxml2.h
EXT_DEP = $(patsubst %, ./src/tinyxml/%, $(_EXT_DEP))

# Video Utils
# _VUTIL_DEP = fade.hpp
# VUTIL_DEP = $(patsubst %, ./src/core/video_utils/%, $(_VUTIL_DEP))

# # Global Headers
# _GLOB_DEP = global.hpp global_actors.hpp
# GLOB_DEP = $(patsubst %, ./src/core/global/%, $(_GLOB_DEP))

# # Mode Headers
# _MOD_DEP = map_utils.hpp map_mode.hpp map_tiles.hpp map_objects.hpp map_sprites.hpp map_events.hpp map_zones.hpp map_combat.hpp
# MOD_DEP = $(patsubst %, ./src/modes/map/%, $(_MOD_DEP))



#### RULES ####
# Links object files and libraries into executable
rpg: $(ENG_OBJ) $(SCENE_OBJ) $(COMP_OBJ) $(TILE_OBJ) $(EXT_OBJ) obj/main.o
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LIBS) $(INCS)

# Compiles engine from /src/core/*
$(ODIR)/%.o: ./src/core/%.cpp $(ENG_DEP)
	$(CXX) -c $< -o $@ $(CXXFLAGS)

# $(ODIR)/%.o: ./src/core/scenes/%.cpp $(CORE_DEP)
# 	$(CXX) -c $< -o $@ $(CXXFLAGS)

$(ODIR)/%.o: ./src/core/scenes/%.cpp $(SCENE_DEP)
	$(CXX) -c $< -o $@ $(CXXFLAGS)

$(ODIR)/%.o: ./src/core/components/%.cpp $(COMP_DEP)
	$(CXX) -c $< -o $@ $(CXXFLAGS)

$(ODIR)/%.o: ./src/core/tilemap/%.cpp $(TILE_DEP)
	$(CXX) -c $< -o $@ $(CXXFLAGS)

$(ODIR)/%.o: ./src/tinyxml/%.cpp $(EXT_DEP)
	$(CXX) -c $< -o $@ $(CXXFLAGS)

# # Compiles engine from /src/core/video_utils/*
# $(ODIR)/%.o: ./src/core/video_utils/%.cpp $(VUTIL_DEP)
# 	$(CXX) -c $< -o $@ $(CXXFLAGS)

# # Compiles engine from /src/core/global/*
# $(ODIR)/%.o: ./src/core/global/%.cpp $(GLOB_DEP)
# 	$(CXX) -c $< -o $@ $(CXXFLAGS)

# # Compiles map mode from /src/modes/map/*
# $(ODIR)/%.o: ./src/modes/map/%.cpp $(MOD_DEP)
# 	$(CXX) -c $< -o $@ $(CXXFLAGS)

# Compiles the main.cpp file and places the object
# file in the obj/ directory.
$(ODIR)/main.o: ./src/main.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS)



#### CLEANUP ####
# Ensures that the clean 'rule' isn't mistaken for
# a file name. If a file called 'clean' were made and
# this line wasn't present, the 'clean' file would not
# compile because it has no prerequisites and would be
# considered up-to-date all the time.
#
# NOTE: Rules only activate if the dependant files
# (right side of colon) have changed since the last time
# the make was run.
.PHONY: clean

# Ran independantly.
# Deletes all object files and the executable.
clean:
	rm -f $(ODIR)/*.o #*~ core ./src/*~
	rm -f rpg
